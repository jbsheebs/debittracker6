package com.umkc.jmsyqf.debittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.umkc.jmsyqf.debittracker.controllers.DateTimeHelper;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.InvalidParameterException;
import com.umkc.jmsyqf.debittracker.objects.Transaction;
import com.umkc.jmsyqf.debittracker.R;

public class NewEditTransactionForm extends ActionBarActivity {

    private String type;
    private String category;
    private String description;
    private String accountOwnerName;
    private int month;
    private int day;
    private int year;
    private int minute;
    private int hour;
    private int second;
    private boolean isCleared = true;
    private String transactionLayoutOwnerName;
    private int imageResourceId;

    private DateTimeHelper dateTimeHelper;

    private boolean typeChanged;

    private Button buttonTransactionValue;
    private Button buttonDate;
    private Button buttonCategory;
    private int resultBalanceInt;
    private DatabaseHelper db;
    private NumberFormat nf;
    private Calendar calendar;
    private ImageView imageViewCategory;
    private final int NUMBER_PAD = 1;
    private final int CATEGORY_SELECTION = 2;
    private final String TRANSACTION_LAYOUT_OWNER_NAME = "TRANSACTION_LAYOUT_NAME";
    private final String TYPE = "type";
    public static final String CATEGORY_RESOURCE_ID = "category_resource_id";
    public static final String CATEGORY_NAME = "category_name";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_edit_transaction_layout);
        //setActionBar();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_edit_transaction);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        nf = NumberFormat.getCurrencyInstance();
        Intent intent = getIntent();
        transactionLayoutOwnerName = intent.getExtras().getString(TRANSACTION_LAYOUT_OWNER_NAME);
        //accountOwnerName = intent.getExtras().getString(TRANSACTION_LAYOUT_OWNER_NAME);
        final Transaction workingTransaction = (Transaction) intent.getExtras().getSerializable("CURRENT_TRANSACTION");

        if (workingTransaction == null){
            TextView title = (TextView)findViewById(R.id.action_bar_title);
            title.setText("New Transaction");
        } else {
            TextView title = (TextView)findViewById(R.id.action_bar_title);
            title.setText("Paid");
        }

        typeChanged = true;

        dateTimeHelper = new DateTimeHelper();
        db = new DatabaseHelper(this);
        //DatabaseGateway databaseGateway = new DatabaseGateway(this);

        final Spinner spinnerType = (Spinner) findViewById(R.id.trans_type);
        final Spinner spinnerAccountName = (Spinner) findViewById(R.id.account_type);

        CheckBox checkBoxCleared = (CheckBox) findViewById(R.id.cbCleared);
        buttonCategory = (Button) findViewById(R.id.btnCategory);
        imageViewCategory = (ImageView) findViewById(R.id.category_image);
        buttonDate = (Button) findViewById(R.id.btnDate);
        buttonTransactionValue = (Button) findViewById(R.id.btnTranVal);
        Button saveButton = (Button) findViewById(R.id.btnSave);
        final TextView descriptionTextView = (TextView) findViewById(R.id.txtDesc);

        ArrayList<String> accountNames = db.getAccountNames();
        ArrayAdapter<String> adapterAccount = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, accountNames);
        adapterAccount.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        spinnerAccountName.setAdapter(adapterAccount);

        // set account spinner to the account that is editing/creating a transaction
        int spinnerAccountPosition = adapterAccount.getPosition(transactionLayoutOwnerName);
        spinnerAccountName.setSelection(spinnerAccountPosition);

        ArrayList<String> types = new ArrayList<>();
        types.add("Expense");
        types.add("Income");

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, types);
        adapterType.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        spinnerType.setAdapter(adapterType);

        // load initial date to be displayed on time button
        loadInitialDate();
        calendar = Calendar.getInstance();
        buttonTransactionValue.setText(nf.format(0));

        // if editing a transaction
        if (workingTransaction != null) {

            buttonTransactionValue.setText(nf.format(workingTransaction.getTransAmount() / 100.0));

            typeChanged = false;

            // set spinner position for type
            int spinnerTypePosition = adapterType.getPosition(workingTransaction.getTransType());
            spinnerType.setSelection(spinnerTypePosition);
            // set spinner position for account
            spinnerAccountPosition = adapterAccount.getPosition(workingTransaction.getOwnerName());
            spinnerAccountName.setSelection(spinnerAccountPosition);
            // set date and time
            month = workingTransaction.getTransMonth();
            day = workingTransaction.getTransDay();
            year = workingTransaction.getTransYear();
            hour = workingTransaction.getTransHour();
            minute = workingTransaction.getTransMinute();
            second = workingTransaction.getTransSecond();
            buttonDate.setText(workingTransaction.getDateAndTimeString());
            // set calendar object for date time picker
            calendar.set(year, month - 1, day, hour, minute, second);
            accountOwnerName = workingTransaction.getOwnerName();
            // set description
            descriptionTextView.setText(workingTransaction.getTransDescription());
            resultBalanceInt = workingTransaction.getTransAmount();
            // set image
            imageResourceId = workingTransaction.getImageResourceId();
            setImageViewCategory(workingTransaction.getTransType(), workingTransaction.getImageResourceId());
            //imageViewCategory.setImageResource(imageResourceId);
            // set category dialog
            String category = workingTransaction.getTransCategory();
            buttonCategory.setText(category);

            isCleared = workingTransaction.isCleared();
            checkBoxCleared.setChecked(isCleared);
        }

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (typeChanged) {
                    String transactionType = parent.getItemAtPosition(position).toString();
                    setImageViewByType(transactionType);
                    buttonCategory.setText("Category");
                    imageResourceId = 0;
                }
                typeChanged = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        checkBoxCleared.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCleared = isChecked;
            }
        });

        buttonCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String transactionType = spinnerType.getSelectedItem().toString();

                Intent i = new Intent(NewEditTransactionForm.this, CategorySelectionGridLayout.class);
                i.putExtra(TYPE, transactionType);
                startActivityForResult(i, CATEGORY_SELECTION);
            }
        });

        buttonTransactionValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewEditTransactionForm.this, NumberPadForm.class);
                i.putExtra("VALUE", 0);
                startActivityForResult(i, NUMBER_PAD);
            }
        });

        final SlideDateTimeListener listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {
                calendar.setTime(date);
                month = (calendar.get(Calendar.MONTH) + 1);
                day = calendar.get(Calendar.DATE);
                year = calendar.get(Calendar.YEAR);
                hour = calendar.get(Calendar.HOUR_OF_DAY);
                minute = calendar.get(Calendar.MINUTE);
                second = calendar.get(Calendar.SECOND);
                // load user selected date to time button
                String currentDateTime = dateTimeHelper.loadDate(month, day, year, hour, minute);
                buttonDate.setText(currentDateTime);
            }
        };

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get date object from calendar for date time picker
                Date date = calendar.getTime();
                SlideDateTimePicker.Builder dateTimePicker = new SlideDateTimePicker.Builder(getSupportFragmentManager());
                dateTimePicker.setListener(listener).setInitialDate(date).build().show();
            }
        });

        // save all values to transaction
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                type = spinnerType.getSelectedItem().toString();
                accountOwnerName = spinnerAccountName.getSelectedItem().toString();
                category = buttonCategory.getText().toString();
                if (category.equals("Category")){
                    category = type;
                }
                description = descriptionTextView.getText().toString();

                if (workingTransaction != null) {

                    Transaction newTransaction = new Transaction(workingTransaction.getTransID(), accountOwnerName, resultBalanceInt, type,
                            imageResourceId, category, description, month, day, year, hour, minute, second, isCleared);

                    // try updating transaction in DB
                    // catch invalid transaction
                    try {
                        db.updateTransaction(newTransaction);
                    } catch (InvalidParameterException e) {
                        e.printStackTrace();
                    }

                } else {

                    Transaction newTransaction = new Transaction(accountOwnerName, resultBalanceInt, type, imageResourceId, category,
                            description, month, day, year, hour, minute, second, isCleared);

                    // try adding new transaction to DB
                    // catch invalid transaction
                    try {
                        db.addTransaction(newTransaction);
                    } catch (InvalidParameterException e) {
                        e.printStackTrace();
                    }
                }

                Intent resultIntent = getIntent();
                // give correct layout back to calling activity
                resultIntent.putExtra(TRANSACTION_LAYOUT_OWNER_NAME, transactionLayoutOwnerName);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    public void loadInitialDate() {

        Calendar now = Calendar.getInstance();
        year = now.get(Calendar.YEAR);
        month = (now.get(Calendar.MONTH) + 1);
        day = now.get(Calendar.DATE);
        minute = now.get(Calendar.MINUTE);
        hour = now.get(Calendar.HOUR_OF_DAY);
        second = now.get(Calendar.SECOND);
        String currentDateTime;
        String amPm;
        int amPmHour;
        if (hour > 12){
            amPmHour  = hour - 12;
            amPm = "PM";
        } else if (hour == 12){
            amPmHour = 12;
            amPm = "PM";
        } else if (hour == 0){
            amPmHour = 12;
            amPm = "AM";
        } else {
            amPmHour = hour;
            amPm = "AM";
        }
        if (minute < 10) {
            currentDateTime = month + "/" + day + "/" + year + " " + amPmHour + ":0" + minute + " " + amPm;
        } else {
            currentDateTime = month + "/" + day + "/" + year + " " + amPmHour + ":" + minute + " " + amPm;
        }
        buttonDate.setText(currentDateTime);

    }

    public void setImageViewCategory(String transactionType, int resourceId){
        if (transactionType.equals("Expense") && resourceId == 0) {
            imageViewCategory.setImageResource(R.drawable._red_right_arrow);
        } else if (transactionType.equals("Income") && resourceId == 0){
            imageViewCategory.setImageResource(R.drawable._green_right_arrow);
        } else {
            imageViewCategory.setImageResource(imageResourceId);
        }
    }

    public void setImageViewByType(String transactionType) {
        if (transactionType.equals("Expense")) {
            imageViewCategory.setImageResource(R.drawable._red_right_arrow);
        } else if (transactionType.equals("Income")) {
            imageViewCategory.setImageResource(R.drawable._green_right_arrow);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == NUMBER_PAD) {
            // get number from number pad
            String result = data.getStringExtra("VALUE");
            int resultInt = Integer.parseInt(result);
            buttonTransactionValue.setText(nf.format(resultInt / 100.0));
            resultBalanceInt = resultInt;
        }
        if (resultCode == RESULT_OK && requestCode == CATEGORY_SELECTION) {
            // get category and image resource

            category = data.getStringExtra(CATEGORY_NAME);
            imageResourceId = data.getIntExtra(CATEGORY_RESOURCE_ID, 0);

            imageViewCategory.setImageResource(imageResourceId);
            buttonCategory.setText(category);

        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

//    public void setActionBar() {
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.edit_transaction_action_bar);
//
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//    }
}