package com.umkc.jmsyqf.debittracker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.umkc.jmsyqf.debittracker.R;

import java.text.NumberFormat;
import java.util.ArrayList;

import com.umkc.jmsyqf.debittracker.objects.Account;

public class AccountAdapter extends BaseAdapter {


    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    public static ArrayList<Account> accountArrayList;
    private static final int ACCOUNTS_TOTAL = 0;
    private static final int ACCOUNTS_INDIVIDUAL = 1;

    private LayoutInflater layoutInflater;

    public AccountAdapter(Context context, ArrayList<Account> results) {
        accountArrayList = results;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type;
        if (position == 0){
            type = ACCOUNTS_TOTAL;
        } else {
            type = ACCOUNTS_INDIVIDUAL;
        }
        return type;
    }

    @Override
    public int getCount() {
        return accountArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return accountArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        int type = getItemViewType(position);
        if (convertView == null) {

            if (type == ACCOUNTS_TOTAL) {

                convertView = layoutInflater.inflate(R.layout.all_accounts_row_view, null);
                holder = new ViewHolder();
                holder.accountBalance = (TextView) convertView.findViewById(R.id.all_accounts_true_balance);
                holder.clearedBalance = (TextView) convertView.findViewById(R.id.all_accounts_cleared_balance);

            } else {

                convertView = layoutInflater.inflate(R.layout.custom_row_view_account, null);
                holder = new ViewHolder();
                holder.accountName = (TextView) convertView.findViewById(R.id.account_name);
                holder.accountBalance = (TextView) convertView.findViewById(R.id.account_balance);
                holder.clearedBalance = (TextView) convertView.findViewById(R.id.account_cleared_balance);
                holder.accountName.setText(accountArrayList.get(position).getAccountName());

            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.accountBalance.setText(nf.format(accountArrayList.get(position).getAccountBalance()/100.0));
        holder.clearedBalance.setText(nf.format(accountArrayList.get(position).getAccountClearedBalance()/100.0));

        return convertView;
    }

    static class ViewHolder {
        TextView accountName;
        TextView accountBalance;
        TextView clearedBalance;
    }
}