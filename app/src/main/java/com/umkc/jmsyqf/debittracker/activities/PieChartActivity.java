package com.umkc.jmsyqf.debittracker.activities;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Transition;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.umkc.jmsyqf.debittracker.R;
import com.umkc.jmsyqf.debittracker.adapters.DateDialogAdapter;
import com.umkc.jmsyqf.debittracker.controllers.DateTimeHelper;
import com.umkc.jmsyqf.debittracker.controllers.ViewGroupUtils;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.PieChartCategoryItems;
import com.umkc.jmsyqf.debittracker.objects.PieModelSliceItems;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.communication.IOnItemFocusChangedListener;
import org.eazegraph.lib.models.PieModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;

public class PieChartActivity extends ActionBarActivity {

    private PieChart mPieChart;

    private DatabaseHelper db;

    private ImageView colorLegend;
    private ImageView categoryIcon;
    private TextView categoryName;
    private TextView textViewAmount;
    private TextView totalAmount;

    private DateTimeHelper dateTimeHelper;
    public static final String DATE_MODE = "date_mode";
    private int sumOfTransactions;
    private NumberFormat nf = NumberFormat.getCurrencyInstance();

    private ArrayList<PieModelSliceItems> pieModelSliceItemsList;
    private ArrayList<PieChartCategoryItems> transactionList;
    private ArrayList<Transaction> tempTransactionList;
    private ArrayList<PieModel> pieModelList;

    private static final boolean IS_EXPENSE = true;
    private static final boolean IS_INCOME = false;
    private boolean activityWasEmpty;

    private static String dateFilterMode;
    private boolean isToggleChecked;

    private Toolbar toolbarBottom;

    public void onCreate(final Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        Transition fade = new Fade();
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        fade.excludeTarget(R.id.main_toolbar_bottom, true);
        getWindow().setExitTransition(fade);
        getWindow().setEnterTransition(fade);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.pie_chart_view_layout);

        db = new DatabaseHelper(this);

        Toolbar toolbarTop = (Toolbar) findViewById(R.id.main_toolbar_top);
        setSupportActionBar(toolbarTop);

        toolbarBottom = (Toolbar) findViewById(R.id.main_toolbar_bottom);
        setSupportActionBar(toolbarBottom);
        toolbarBottom.setTransitionName("action_bar");

        ImageView pieChartView = (ImageView)findViewById(R.id.pie_chart_view);
        pieChartView.setImageResource(R.mipmap.pie_chart_selected);

        ImageView searchView = (ImageView)findViewById(R.id.search_view);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSearchViewActivity();
            }
        });

        ImageView mainListView = (ImageView)findViewById(R.id.main_list_view);
        mainListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainAccountListView();
            }
        });

        ImageView exportView = (ImageView)findViewById(R.id.export_view);
        exportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToExportViewActivity();
            }
        });

        dateFilterMode = "MONTH";

        dateTimeHelper = new DateTimeHelper(dateFilterMode);
        String monthYearString = dateTimeHelper.getMonthAndYearString();
        final TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(monthYearString);

        final ArrayList<String> dateOptionsList = dateTimeHelper.getDateTimeOptions();

        final Dialog dateDialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.date_options_list_view, null);
        final ListView dateListView = (ListView)view.findViewById(R.id.custom_list_view);

        dateListView.setAdapter(new DateDialogAdapter(this, dateOptionsList));
        dateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dateDialog.setContentView(view);
        WindowManager.LayoutParams params = dateDialog.getWindow().getAttributes();
        params.gravity = Gravity.TOP | Gravity.CENTER;

        bindVariables();

        ToggleButton incomeExpenseToggle = (ToggleButton) findViewById(R.id.expense_income_toggle);

        // set true to get expenses for transaction list
        incomeExpenseToggle.setChecked(IS_EXPENSE);
        isToggleChecked = IS_EXPENSE;
        transactionList = new ArrayList<>();

        tempTransactionList = dateTimeHelper.getFilteredTransactionListByDate(db.getAllTransactionBasedOnIncomeOrExpense(IS_EXPENSE), dateFilterMode);
        ArrayList<String> categoryList = groupTransactionsByCategory(tempTransactionList);

        getCategoryAmountAndImageId(categoryList);

        sumOfTransactions = getTotalAmountOfTransactions(tempTransactionList);
        totalAmount = (TextView)findViewById(R.id.tf_total_amount);
        totalAmount.setText(nf.format(sumOfTransactions/100.0));

        // first view when entering activity
        if (transactionList.isEmpty()) {

            activityWasEmpty = true;
            View chartView = findViewById(R.id.pie_chart_view_relative_layout);
            LayoutInflater inflater = getLayoutInflater();
            View parentView = inflater.inflate(R.layout.pie_chart_view_no_data, null);
            View noChartView = parentView.findViewById(R.id.pie_chart_view_relative_layout_no_data);
            ViewGroupUtils.replaceView(chartView, noChartView);

        } else {

            setIncomeOrExpense(isToggleChecked);
            setSlices();
            setLegend(isToggleChecked);
        }

        mPieChart.setOnItemFocusChangedListener(new IOnItemFocusChangedListener() {
            @Override
            public void onItemFocusChanged(int i) {
                setLegend(isToggleChecked);
            }
        });

        incomeExpenseToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isToggleChecked = isChecked;
                setPieChartAccordingToDates(isChecked);
            }
        });

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog.show();
            }
        });

        dateListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dateFilterMode = dateOptionsList.get(position);
                dateDialog.dismiss();
                DateTimeHelper dateTimeHelper = new DateTimeHelper();
                title.setText(dateTimeHelper.getCurrentDateRange(dateFilterMode));
                setPieChartAccordingToDates(isToggleChecked);
            }
        });

        ImageView leftArrow = (ImageView)findViewById(R.id.left_arrow);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title.setText(dateTimeHelper.getPreviousDateRange(dateFilterMode));
                setPieChartAccordingToDates(isToggleChecked);

            }
        });

        ImageView rightArrow = (ImageView)findViewById(R.id.right_arrow);
        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title.setText(dateTimeHelper.getNextDateRange(dateFilterMode));
                setPieChartAccordingToDates(isToggleChecked);

            }
        });
    }

    public void getCategoryAmountAndImageId(ArrayList<String> categoryList){
        int categoryAmount;
        for (String category : categoryList){
            categoryAmount = 0;
            int imageId = 0;
            for (Transaction transaction : tempTransactionList){
                if (category.equals(transaction.getTransCategory())){
                    imageId = transaction.getImageResourceId();
                    categoryAmount += transaction.getTransAmount();
                }
            }
            PieChartCategoryItems pieChartCategoryItems = new PieChartCategoryItems(category, categoryAmount, imageId);
            transactionList.add(pieChartCategoryItems);
        }
    }

    public int getTotalAmountOfTransactions(ArrayList<Transaction> transList){
        int totalAmount = 0;
        for (Transaction transaction : transList){
            int amount = transaction.getTransAmount();
            totalAmount += amount;
        }
        return totalAmount;
    }

    public void setPieChartAccordingToDates(boolean isChecked) {

        mPieChart.clearChart();
        transactionList = new ArrayList<>();

        ArrayList<Transaction> tl = db.getAllTransactionBasedOnIncomeOrExpense(isChecked);
        tempTransactionList = dateTimeHelper.getFilteredTransactionListByDate(tl, dateFilterMode);

        ArrayList<String> categoryList = groupTransactionsByCategory(tempTransactionList);

        getCategoryAmountAndImageId(categoryList);
        sumOfTransactions = getTotalAmountOfTransactions(tempTransactionList);
        //sumOfTransactions = db.getTotalAmountOfTransactionsForTransactionType(isChecked);

        // if transaction list is not empty and...
        if (!transactionList.isEmpty()) {
            // if previous layout was empty then we need to change the layout because this one won't be
            if (activityWasEmpty) {

                activityWasEmpty = false;
                View noChartView = findViewById(R.id.pie_chart_view_relative_layout_no_data);
                LayoutInflater inflater = getLayoutInflater();
                View parentView = inflater.inflate(R.layout.pie_chart_view_layout, null);
                View chartView = parentView.findViewById(R.id.pie_chart_view_relative_layout);
                ViewGroupUtils.replaceView(noChartView, chartView);
                bindVariables();
                setIncomeOrExpense(isChecked);
                setLegend(isToggleChecked);
                setSlices();

            // if previous activity wasn't empty then we shouldn't need to change the layout
            } else {

                activityWasEmpty = false;
                bindVariables();
                setIncomeOrExpense(isChecked);
                setSlices();
                setLegend(isToggleChecked);
            }

            mPieChart.setOnItemFocusChangedListener(new IOnItemFocusChangedListener() {
                @Override
                public void onItemFocusChanged(int i) {
                    setLegend(isToggleChecked);
                }
            });

        // if transaction list is empty
        } else {

            // if previous activity was empty then we don't need to change the layout
            if (activityWasEmpty) {

                activityWasEmpty = true;
                return;

            // if previous activity wasn't empty then we need to change the layout
            } else {

                activityWasEmpty = true;
                View chartView = findViewById(R.id.pie_chart_view_relative_layout);
                LayoutInflater inflater = getLayoutInflater();
                View parentView = inflater.inflate(R.layout.pie_chart_view_no_data, null);
                View noChartView = parentView.findViewById(R.id.pie_chart_view_relative_layout_no_data);
                ViewGroupUtils.replaceView(chartView, noChartView);
            }
        }
    }

    public ArrayList<String> groupTransactionsByCategory(ArrayList<Transaction> transList) {
        HashSet<String> categorySet = new HashSet<>();
        for (Transaction transaction : transList){
            String uniqueCategory = transaction.getTransCategory();
            categorySet.add(uniqueCategory);
        }
        ArrayList<String> categoryList = new ArrayList<>();
        categoryList.addAll(categorySet);
        return categoryList;
    }

    public void bindVariables() {

        totalAmount = (TextView)findViewById(R.id.tf_total_amount);
        colorLegend = (ImageView) findViewById(R.id.color_legend);
        categoryIcon = (ImageView) findViewById(R.id.image_view_category);
        categoryName = (TextView) findViewById(R.id.tf_category);
        textViewAmount = (TextView) findViewById(R.id.tf_amount);
        mPieChart = (PieChart) findViewById(R.id.pie_chart_eazegraph);
        mPieChart.setAutoCenterInSlice(false);
        totalAmount.setText(nf.format(sumOfTransactions/100.0));

    }

    public void setSlices() {

        for (int i = 0; i < pieModelList.size(); i++) {
            mPieChart.addPieSlice(pieModelList.get(i));
        }
    }

    public void setIncomeOrExpense(boolean isExpense) {

        pieModelSliceItemsList = new ArrayList<>();
        pieModelList = new ArrayList<>();
        ArrayList<String> colorList = dateTimeHelper.getColors(isExpense);

        for (int i = 0; i < transactionList.size(); i++) {
            int imageResourceId = transactionList.get(i).imageResourceId;
            String category = transactionList.get(i).category;
            int amount = transactionList.get(i).amount;
            pieModelSliceItemsList.add(new PieModelSliceItems(colorList.get(i), amount, category, imageResourceId));
            pieModelList.add(new PieModel(category, amount, Color.parseColor(colorList.get(i))));
        }
    }

    public void setLegend(boolean isExpense) {

        int currentItem = mPieChart.getCurrentItem();

        String itemColor = pieModelSliceItemsList.get(currentItem).getColor();
        String itemAmount = pieModelSliceItemsList.get(currentItem).getAmount();
        String itemCategory = pieModelSliceItemsList.get(currentItem).getCategory();
        int imageResourceId = pieModelSliceItemsList.get(currentItem).getImageResourceId();

        if (imageResourceId == 0) {
            if (isExpense) {
                categoryIcon.setImageResource(R.drawable._red_right_arrow);
            } else {
                categoryIcon.setImageResource(R.drawable._green_right_arrow);
            }
        } else {
            categoryIcon.setImageResource(imageResourceId);
        }
        colorLegend.setColorFilter(Color.parseColor(itemColor));
        categoryName.setText(itemCategory);
        textViewAmount.setText(itemAmount);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToSearchViewActivity(){
        Intent i = new Intent(PieChartActivity.this, SearchResultsActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void goToMainAccountListView(){
        Intent i = new Intent(PieChartActivity.this, MainAccountListViewLayout.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void goToExportViewActivity(){
        Intent i = new Intent(PieChartActivity.this, ExportTransactionsActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }
}