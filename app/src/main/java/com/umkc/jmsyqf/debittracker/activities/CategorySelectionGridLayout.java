package com.umkc.jmsyqf.debittracker.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import com.umkc.jmsyqf.debittracker.adapters.CustomListViewDialogAdapter;
import com.umkc.jmsyqf.debittracker.adapters.GridViewAdapter;
import com.umkc.jmsyqf.debittracker.controllers.CategoryHelper;
import com.umkc.jmsyqf.debittracker.objects.ImageItem;
import com.umkc.jmsyqf.debittracker.R;

public class CategorySelectionGridLayout extends ActionBarActivity {


    private ImageItem selectedItemMain;
    private CategoryHelper categoryHelper;
    private ArrayList<ImageItem> categoryList;

    private String categoryName;
    private int categoryResourceId;

    public static final String CATEGORY_RESOURCE_ID = "category_resource_id";
    public static final String CATEGORY_NAME = "category_name";
    private final String TYPE = "type";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        categoryHelper = new CategoryHelper(this);
        final String type = intent.getStringExtra(TYPE);
        setContentView(R.layout.transaction_grid_view);

        GridView gridView = (GridView) findViewById(R.id.grid_view_transactions);
        GridViewAdapter gridViewAdapter = null;

        if (type.equals("Expense")) {
            gridView.setNumColumns(5);
            categoryList = categoryHelper.getExpenseCategories();
            gridViewAdapter = new GridViewAdapter(this, categoryList);
        }
        else if (type.equals("Income")){
            gridView.setNumColumns(4);
            categoryList = categoryHelper.getIncomeCategories();
            gridViewAdapter = new GridViewAdapter(this, categoryList);
        }

        gridView.setAdapter(gridViewAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (type.equals("Expense")) {
                    selectedItemMain = categoryList.get(position);
                    getSubMenuOptions(selectedItemMain);
                } else if (type.equals("Income")) {
                    selectedItemMain = categoryList.get(position);
                    categoryName = selectedItemMain.getCategoryName();
                    categoryResourceId = selectedItemMain.getResourceId();
                    Intent resultIntent = getIntent();
                    resultIntent.putExtra(CATEGORY_NAME, categoryName);
                    resultIntent.putExtra(CATEGORY_RESOURCE_ID, categoryResourceId);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            }
        });
    }

    private void getSubMenuOptions(ImageItem item) {

        categoryName = item.getCategoryName();

        final Dialog dialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_custom_list_view_subcategories, null);
        final ListView listView = (ListView)view.findViewById(R.id.custom_list_view);

        final ArrayList<ImageItem> subCategoryList = categoryHelper.getSubCategoryList(categoryName);

        if (subCategoryList.isEmpty()){

            categoryResourceId = selectedItemMain.getResourceId();
            Intent resultIntent = getIntent();
            resultIntent.putExtra(CATEGORY_NAME, categoryName);
            resultIntent.putExtra(CATEGORY_RESOURCE_ID, categoryResourceId);
            setResult(RESULT_OK, resultIntent);
            dialog.dismiss();
            finish();
        }

        listView.setAdapter(new CustomListViewDialogAdapter(this, subCategoryList));

        TextView title = (TextView)view.findViewById(R.id.sub_menu_title);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        title.setText(categoryName);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ImageItem selectedItemSub = subCategoryList.get(position);
                categoryResourceId = selectedItemSub.getResourceId();
                String totalCategoryTitle = categoryName + ": " + selectedItemSub.getCategoryName();
                Intent resultIntent = getIntent();
                resultIntent.putExtra(CATEGORY_NAME, totalCategoryTitle);
                resultIntent.putExtra(CATEGORY_RESOURCE_ID, categoryResourceId);
                setResult(RESULT_OK, resultIntent);
                dialog.dismiss();
                finish();

            }
        });
        if (!subCategoryList.isEmpty()) {
            dialog.show();
        }
    }
}