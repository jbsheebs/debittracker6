package com.umkc.jmsyqf.debittracker.objects;

import java.io.Serializable;

public class Transaction implements Serializable {

    private int transID;
    private String ownerName;
    private String transType;
    private String transCategory;
    private String transDescription;
    private int transAmount;
    private int totalBalance;
    private int transMinute;
    private int transHour;
    private int transYear;
    private int transDay;
    private int transMonth;
    private int transSecond;
    private int imageResourceId;
    private boolean isCleared;

    public Transaction(int id, String owner, int amount, String type, int resourceId, String category, String description,
                int month, int day, int year, int hour, int minute, int second, boolean cleared){
        transID = id;
        transMinute = minute;
        transHour = hour;
        transDay = day;
        transMonth = month;
        transYear = year;
        transSecond = second;
        ownerName = owner;
        transAmount = amount;
        transType = type;
        transCategory = category;
        transDescription = description;
        imageResourceId = resourceId;
        isCleared = cleared;
    }

    public Transaction(String owner, int amount, String type, int resourceId, String category, String description, int total,
                       int month, int day, int year, int hour, int minute, int second, boolean cleared){
        transMinute = minute;
        transHour = hour;
        transDay = day;
        transMonth = month;
        transYear = year;
        transSecond = second;
        ownerName = owner;
        transAmount = amount;
        transType = type;
        transCategory = category;
        transDescription = description;
        totalBalance = total;
        imageResourceId = resourceId;
        isCleared = cleared;
    }

    public Transaction(String owner, int amount, String type, int resourceId, String category, String description,
                       int month, int day, int year, int hour, int minute, int second, boolean cleared){
        transMinute = minute;
        transHour = hour;
        transDay = day;
        transMonth = month;
        transYear = year;
        transSecond = second;
        ownerName = owner;
        transAmount = amount;
        transType = type;
        transCategory = category;
        transDescription = description;
        imageResourceId = resourceId;
        isCleared = cleared;
    }

    Transaction(){}

    public void setTransSecond(int second) {
        transSecond = second;
    }

    public int getTransMonth() {
        return transMonth;
    }

    public int getTransDay() {
        return transDay;
    }

    public int getTransYear() {
        return transYear;
    }

    public int getTransHour() {
        return transHour;
    }

    public int getTransMinute() {
        return transMinute;
    }

    public int getTransID() {
        return transID;
    }

    public String getDateAndTimeString(){
        String amPm = "AM";
        int amPmHour = 0;
        if (transHour > 12){
            amPmHour = transHour - 12;
            amPm = "PM";
        } else if (transHour == 12){
            amPmHour = transHour;
            amPm = "PM";
        } else if (transHour == 0){
            amPmHour = 12;
            amPm = "AM";
        } else {
            amPmHour = transHour;
            amPm = "AM";
        }
        if (transMinute < 10) {
            return transMonth + "/" + transDay + "/" + transYear + " " + amPmHour + ":0" + transMinute + " " + amPm;
        } else {
            return transMonth + "/" + transDay + "/" + transYear + " " + amPmHour + ":" + transMinute + " " + amPm;
        }
    }

    public boolean isCleared() {
        return isCleared;
    }

    public void setCleared(boolean isCleared) {
        this.isCleared = isCleared;
    }

    public int getTransAmount(){
        return transAmount;
    }

    public String getTransType(){
        return transType;
    }

    public String getTransCategory(){
        return transCategory;
    }

    public String getTransDescription(){
        return transDescription;
    }

    public String getDescriptionWithParens(){
        if (!transDescription.isEmpty()){
            return "(" + transDescription + ")";
        }
        else return "";
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setTransID(int id) {
        transID = id;
    }

    public void setOwnerName(String owner){
        ownerName = owner;
    }

    public void setTransMonth(int month){
        transMonth = month;
    }

    public void setTransDay(int day){
        transDay = day;
    }

    public void setTransYear(int year){
        transYear = year;
    }

    public void setTransHour(int hour){
        transHour = hour;
    }

    public void setTransMinute(int minute){
        transMinute = minute;
    }

    public void setTransAmount(int amount){
        transAmount = amount;
    }

    public void setTransType(String type){
        transType = type;
    }

    public void setTransCategory(String cat){
        transCategory = cat;
    }

    public void setTransDescription(String description){
        transDescription = description;
    }

    public int getTotalBalance() {
        return totalBalance;
    }

    public boolean isExpenseType(){
        return transType.equals("Expense");
    }

    public void setTotalBalance(int tb) {
        totalBalance = tb;
    }

    public int getTransSecond() {
        return transSecond;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }
}
