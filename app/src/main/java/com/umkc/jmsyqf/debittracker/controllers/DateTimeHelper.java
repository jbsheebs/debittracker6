package com.umkc.jmsyqf.debittracker.controllers;

import android.support.v4.util.CircularArray;
import android.util.Log;

import com.umkc.jmsyqf.debittracker.objects.Transaction;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class DateTimeHelper {

    private String mode;

    private static int monthFilter;
    private static int yearFilter;

    private static int dayFilterStart;
    private static int monthFilterStart;
    private static int yearFilterStart;

    private static int dayFilterEnd;
    private static int monthFilterEnd;
    private static int yearFilterEnd;

    private static Calendar now;
    private static Calendar calendarStart;
    private static Calendar calendarEnd;

    private static ArrayList<Transaction> transactionList;

    public DateTimeHelper() {
    }

    public DateTimeHelper(String dateSelectionMode) {
        this.mode = dateSelectionMode;
        DateTimeHelper.now = Calendar.getInstance();
        DateTimeHelper.monthFilter = (now.get(Calendar.MONTH) + 1);
        DateTimeHelper.yearFilter = (now.get(Calendar.YEAR));
    }

    public ArrayList<Transaction> getFilteredTransactionListByDate(ArrayList<Transaction> list, String dateSelectionMode) {
        this.mode = dateSelectionMode;
        DateTimeHelper.transactionList = list;
        switch (mode) {
            case "WEEK":
                return filterTransactionListByWeek();
            case "YEAR":
                return filteredTransactionListByYear();
            default:
                return filterTransactionListByMonth();
        }
    }

    public String getCurrentDateRange(String dateSelectionMode) {
        Calendar now = Calendar.getInstance();
        monthFilter = (now.get(Calendar.MONTH) + 1);
        yearFilter = now.get(Calendar.YEAR);
        this.mode = dateSelectionMode;
        switch (mode) {
            case "WEEK":
                getWeekRange();
                return getWeekString(true);
            case "YEAR":
                return String.valueOf(yearFilter);
            default:
                return getMonth(monthFilter) + " " + yearFilter;
        }
    }

    public String getPreviousDateRange(String dateSelectionMode) {
        this.mode = dateSelectionMode;
        switch (mode) {
            case "WEEK":
                getPreviousWeekRange();
                return getWeekString(false);
            case "YEAR":
                return getYear(false);
            default:
                return getMonth(monthFilter - 1) + " " + yearFilter;
        }
    }

    public String getNextDateRange(String dateSelectionMode) {
        this.mode = dateSelectionMode;
        switch (mode) {
            case "WEEK":
                getNextWeekRange();
                return getWeekString(true);
            case "YEAR":
                return getYear(true);
            default:
                return getMonth(monthFilter + 1) + " " + yearFilter;
        }
    }

    public String getMonthAndYearString() {
        return getMonth(monthFilter) + " " + yearFilter;
    }

    public String getYearString() {
        return String.valueOf(yearFilter);
    }

    public String getWeekString(boolean next) {
        // need to catch if # < 2011
        int yearStart = yearFilterStart % 100;
        int yearEnd = yearFilterEnd % 100;
        return (monthFilterStart + 1) + "/" + dayFilterStart + "/" + yearStart + " - " + (monthFilterEnd + 1) + "/" + dayFilterEnd + "/" + yearEnd;
    }

    public String getYear(boolean next) {
        int year = yearFilter;
        if (next) {
            year++;
        } else {
            year--;
        }
        yearFilter = year;
        return String.valueOf(year);
    }

    public void getNextWeekRange() {

        calendarStart.add(Calendar.DATE, 7);
        calendarEnd.add(Calendar.DATE, 7);

        monthFilterStart = calendarStart.get(Calendar.MONTH);
        dayFilterStart = calendarStart.get(Calendar.DATE);
        yearFilterStart = calendarStart.get(Calendar.YEAR);

        monthFilterEnd = (calendarEnd.get(Calendar.MONTH));
        dayFilterEnd = calendarEnd.get(Calendar.DATE);
        yearFilterEnd = calendarEnd.get(Calendar.YEAR);

    }

    public void getPreviousWeekRange() {

        calendarStart.add(Calendar.DATE, -7);
        calendarEnd.add(Calendar.DATE, -7);

        monthFilterStart = calendarStart.get(Calendar.MONTH);
        dayFilterStart = calendarStart.get(Calendar.DATE);
        yearFilterStart = calendarStart.get(Calendar.YEAR);

        monthFilterEnd = (calendarEnd.get(Calendar.MONTH));
        dayFilterEnd = calendarEnd.get(Calendar.DATE);
        yearFilterEnd = calendarEnd.get(Calendar.YEAR);

    }

    public void getWeekRange() {

        Date date = new Date();
        Calendar calendar = Calendar.getInstance(Locale.US);
        calendar.setTime(date);
        int i = calendar.get(Calendar.DAY_OF_WEEK) - calendar.getFirstDayOfWeek();
        calendar.add(Calendar.DATE, -i);
        Date startDate = calendar.getTime();
        calendar.add(Calendar.DATE, 6);
        Date endDate = calendar.getTime();
        calendarStart = Calendar.getInstance();
        calendarEnd = Calendar.getInstance();
        calendarStart.setTime(startDate);
        calendarEnd.setTime(endDate);

        monthFilterStart = calendarStart.get(Calendar.MONTH);
        dayFilterStart = calendarStart.get(Calendar.DATE);
        yearFilterStart = calendarStart.get(Calendar.YEAR);

        monthFilterEnd = calendarEnd.get(Calendar.MONTH);
        dayFilterEnd = calendarEnd.get(Calendar.DATE);
        yearFilterEnd = calendarEnd.get(Calendar.YEAR);
    }


    public ArrayList<Transaction> filterTransactionListByMonth() {
        ArrayList<Transaction> newList = new ArrayList<>();
        for (Transaction t : transactionList) {
            int month = t.getTransMonth();
            int year = t.getTransYear();
            if (month == monthFilter && year == yearFilter) {
                newList.add(t);
            }
        }
        return newList;
    }

//    public ArrayList<Transaction> filterTransactionListByWeek() {
//        ArrayList<Transaction> newList = new ArrayList<>();
//        for (Transaction t : transactionList) {
//            int day = t.getTransDay();
//            int month = (t.getTransMonth() - 1);
//            int year = t.getTransYear();
//            if ((day >= dayFilterStart && month >= monthFilterStart && year >= yearFilterStart)
//                    && (day <= dayFilterEnd && month <= monthFilterEnd && year <= yearFilterEnd)) {
//                newList.add(t);
//            }
//        }
//        return newList;
//    }

    public ArrayList<Transaction> filterTransactionListByWeek(){
        ArrayList<Transaction> newList = new ArrayList<>();
        Calendar startDate = Calendar.getInstance();
        startDate.set(yearFilterStart, monthFilterStart, dayFilterStart, 0, 0, 0);
        Log.e("Start date:", startDate.getTime().toString());

        Calendar endDate = Calendar.getInstance();
        endDate.set(yearFilterEnd, monthFilterEnd, dayFilterEnd, 0, 0, 0);
        Log.e("End date:", endDate.getTime().toString());

        Calendar transactionDate = Calendar.getInstance();
        for (Transaction t : transactionList){
            transactionDate.set(t.getTransYear(), t.getTransMonth() - 1, t.getTransDay(), 0, 0, 0);
            Log.e("Transaction date:", transactionDate.getTime().toString());

            if ((transactionDate.after(startDate) || transactionDate.equals(startDate))
                    && (transactionDate.before(endDate) || transactionDate.equals(endDate))){
                newList.add(t);
            }
        }
        return newList;
    }

    public ArrayList<Transaction> filteredTransactionListByYear() {
        ArrayList<Transaction> newList = new ArrayList<>();
        for (Transaction t : transactionList) {
            if (t.getTransYear() == yearFilter) {
                newList.add(t);
            }
        }
        return newList;
    }

    public String getMonth(int month) {
        Map<Integer, String> months = new HashMap<>();
        months.put(1, "January");
        months.put(2, "February");
        months.put(3, "March");
        months.put(4, "April");
        months.put(5, "May");
        months.put(6, "June");
        months.put(7, "July");
        months.put(8, "August");
        months.put(9, "September");
        months.put(10, "October");
        months.put(11, "November");
        months.put(12, "December");
        if (month == 13) {
            int year = yearFilter;
            year++;
            yearFilter = year;
            month = 1;
        }
        if (month == 0) {
            int year = yearFilter;
            year--;
            yearFilter = year;
            month = 12;
        }
        // assert return month int is 1 - 12
        Assert.assertTrue("Month should be between 1 and 12", (month > 0 && month < 13));
        monthFilter = month;
        return months.get(month);
    }

    public String loadDate(int month, int day, int year, int hour, int minute) {
        String currentDateTime;
        String amPm;
        if (hour > 12) {
            hour = hour - 12;
            amPm = "PM";
        } else if (hour == 12) {
            amPm = "PM";
        } else if (hour == 0) {
            hour = 12;
            amPm = "AM";
        } else {
            amPm = "AM";
        }
        if (minute < 10) {
            currentDateTime = month + "/" + day + "/" + year + " " + hour + ":0" + minute + " " + amPm;
        } else {
            currentDateTime = month + "/" + day + "/" + year + " " + hour + ":" + minute + " " + amPm;
        }
        return currentDateTime;
    }

    public ArrayList<String> getDateTimeOptions() {
        ArrayList<String> dateOptionsList = new ArrayList<>();
        dateOptionsList.add("WEEK");
        dateOptionsList.add("MONTH");
        dateOptionsList.add("YEAR");
        return dateOptionsList;
    }

    public ArrayList<String> getColors(boolean isExpense) {
        ArrayList<String> colorList = new ArrayList<>();
        colorList.add("#3D59AB");
        colorList.add("#6495ED");
        colorList.add("#B0C4DE");
        colorList.add("#C6E2FF");
        colorList.add("#1874CD");
        colorList.add("#87CEFA");
        colorList.add("#BFEFFF");
        colorList.add("#53868B");
        colorList.add("#00C5CD");
        colorList.add("#AEEEEE");
        colorList.add("#48D1CC");
        colorList.add("#03A89E");
        colorList.add("#00C78C");
        colorList.add("#7FFFD4");
        colorList.add("#00CD66");
        colorList.add("#008B45");
        colorList.add("#32CD32");
        colorList.add("#006400");
        colorList.add("#458B00");
        colorList.add("#007f00");
        colorList.add("#004c00");
        colorList.add("#7FFFD4");
        colorList.add("#00CD66");
        colorList.add("#008B45");
        colorList.add("#32CD32");
        colorList.add("#006400");
        colorList.add("#458B00");
        colorList.add("#007f00");
        colorList.add("#004c00");
        colorList.add("#0000CD");
        colorList.add("#003EFF");
        colorList.add("#003F87");
        colorList.add("#004F00");
        colorList.add("#006633");
        colorList.add("#006B54");
        colorList.add("#007FFF");
        colorList.add("#008080");
        colorList.add("#0099CC");
        colorList.add("#00BFFF");
        colorList.add("#20BF9F");
        colorList.add("#1C86EE");
        colorList.add("#3B4990");
        colorList.add("#37BC61");
        if (isExpense) {
            return colorList;
        } else {
            Collections.reverse(colorList);
            return colorList;
        }
    }
}
