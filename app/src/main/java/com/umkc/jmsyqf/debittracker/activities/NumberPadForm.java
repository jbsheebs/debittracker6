package com.umkc.jmsyqf.debittracker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import junit.framework.Assert;

import com.umkc.jmsyqf.debittracker.R;

import java.text.DecimalFormat;

public class NumberPadForm extends Activity implements View.OnClickListener {

    int DialogID;
    String chars;
    int currentChar;
    int val;
    TextView valString;
    double d2;
    double d1;
    String finalBalance;

    private void sendResult() {

        finalBalance = this.chars;
        Intent resultIntent = getIntent();
        if (finalBalance.isEmpty()){
            resultIntent.putExtra("VALUE", "000");
        }
        else {
            resultIntent.putExtra("VALUE", finalBalance);
        }
        setResult(RESULT_OK, resultIntent);
        finish();
    }


    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.number_pad);
        try {
            this.DialogID = getIntent().getExtras().getInt("DialogID");
            (findViewById(R.id.btn1)).setOnClickListener(this);
            (findViewById(R.id.btn2)).setOnClickListener(this);
            (findViewById(R.id.btn3)).setOnClickListener(this);
            (findViewById(R.id.btn4)).setOnClickListener(this);
            (findViewById(R.id.btn5)).setOnClickListener(this);
            (findViewById(R.id.btn6)).setOnClickListener(this);
            (findViewById(R.id.btn7)).setOnClickListener(this);
            (findViewById(R.id.btn8)).setOnClickListener(this);
            (findViewById(R.id.btn9)).setOnClickListener(this);
            (findViewById(R.id.btn0)).setOnClickListener(this);
            (findViewById(R.id.btnOK)).setOnClickListener(this);
            (findViewById(R.id.btnDEL)).setOnClickListener(this);
            this.valString = ((TextView) findViewById(R.id.TextView01));
            this.chars = "";
            this.currentChar = 0;
            this.val = 0;
            this.valString.setText("0.00");
            return;
        } catch (Exception localException) {
            for (;;) {
                this.DialogID = -1;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOK:
                sendResult();
                break;
            case R.id.btnDEL:
                addChar("-1");
                break;
            case R.id.btn0:
                addChar("0");
                break;
            case R.id.btn1:
                addChar("1");
                break;
            case R.id.btn2:
                addChar("2");
                break;
            case R.id.btn3:
                addChar("3");
                break;
            case R.id.btn4:
                addChar("4");
                break;
            case R.id.btn5:
                addChar("5");
                break;
            case R.id.btn6:
                addChar("6");
                break;
            case R.id.btn7:
                addChar("7");
                break;
            case R.id.btn8:
                addChar("8");
                break;
            case R.id.btn9:
                addChar("9");
                break;
        }
    }

    private void addChar(String paramString) {

        DecimalFormat localDecimalFormat = new DecimalFormat("#0.00");
        if (paramString != "-1") {
            if (this.chars.length() < 10) {
                this.chars += paramString;
                this.currentChar = (1 + this.currentChar);
                d2 = Double.parseDouble(this.chars) / 100.0D;
                // validate value is not negative
                Assert.assertTrue(d2 > -1);
                this.valString.setText(localDecimalFormat.format(d2));
            }
        } else {
            if (this.chars.length() <= 0){
                return;
            }
            this.currentChar -= 1;
            this.chars = this.chars.substring(0, this.currentChar);
            if (this.chars.length() == 0) {
                this.valString.setText("0.00");
                return;
            }
            d1 = Double.parseDouble(this.chars) / 100D;
            // validate value is not negative
            Assert.assertTrue(d1 > -1);
            this.valString.setText(localDecimalFormat.format(d1));
        }
    }
}

