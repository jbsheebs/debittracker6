package com.umkc.jmsyqf.debittracker.objects;

public class Account {

    private int accountID;
    private String accountName;
    private int accountBalance;

    private int accountClearedBalance;

    public Account(){}

    Account(int ID, String name, int balance){
        accountID = ID;
        accountName = name;
        accountBalance = balance;
    }

    public Account(String name, int balance, int clearedBalance){
        accountName =  name;
        accountBalance = balance;
        accountClearedBalance = clearedBalance;
    }

    Account(String name){
        accountName =  name;
        accountBalance = 0;
    }

    public int getAccountClearedBalance() {
        return accountClearedBalance;
    }

    public void setAccountClearedBalance(int accountClearedBalance) {
        this.accountClearedBalance = accountClearedBalance;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public void setAccountName(String name){
        accountName = name;
    }

    public void setAccountTotalBalance(int balance){
        accountBalance = balance;
    }

    public String getAccountName(){
        return accountName;
    }

    public int getAccountBalance(){
        return accountBalance;
    }

    public String getBalanceToString(){
        return String.valueOf(accountBalance);
    }


}
