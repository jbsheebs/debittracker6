package com.umkc.jmsyqf.debittracker.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.umkc.jmsyqf.debittracker.R;
import com.umkc.jmsyqf.debittracker.objects.ImageItem;

import java.util.ArrayList;

public class CustomListViewDialogAdapter extends ArrayAdapter<ImageItem>{

    private ArrayList<ImageItem> subCategoryList;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListViewDialogAdapter(Context context, ArrayList<ImageItem> results) {
        super(context, R.layout.custom_list_row_dialog, results);
        this.subCategoryList = new ArrayList<>();
        this.subCategoryList.addAll(results);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public ImageItem getItem(int position){
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_list_row_dialog, parent, false);
            holder = new ViewHolder();
            holder.imageIcon = (ImageView) convertView.findViewById(R.id.custom_list_view_image);
            holder.subCategoryName = (TextView) convertView.findViewById(R.id.custom_list_view_text);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        ImageItem item = subCategoryList.get(position);
        holder.imageIcon.setImageResource(item.getResourceId());
        holder.subCategoryName.setText(item.getCategoryName());
        return convertView;
    }

    static class ViewHolder {
        TextView subCategoryName;
        ImageView imageIcon;
    }
}
