package com.umkc.jmsyqf.debittracker.controllers;

import android.content.Context;

import com.umkc.jmsyqf.debittracker.R;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.ImageItem;

import java.util.ArrayList;

public class CategoryHelper {

    private Context context;
    private DatabaseHelper db;

    public CategoryHelper(Context context){
        this.context = context;
        db = new DatabaseHelper(context);
    }

    public CategoryHelper(){}

    public ArrayList<ImageItem> getArrayListOfMainCategories(){

        ArrayList<ImageItem> mainImageItems = new ArrayList<>();

        // expenses
        mainImageItems.add(new ImageItem(R.drawable._cash_expense, "Other Expense", true));
        mainImageItems.add(new ImageItem(R.drawable._car, "Car", true));
        mainImageItems.add(new ImageItem(R.drawable._debt, "Debt", true));
        mainImageItems.add(new ImageItem(R.drawable._cheeseburger, "Eating Out", true));
        mainImageItems.add(new ImageItem(R.drawable._roller_coaster, "Entertainment", true));


        mainImageItems.add(new ImageItem(R.drawable._family_and_personal, "Family and Personal", true));
        mainImageItems.add(new ImageItem(R.drawable._house, "Home", true));
        mainImageItems.add(new ImageItem(R.drawable._household, "Household and Food", true));
        mainImageItems.add(new ImageItem(R.drawable._insurance, "Insurance", true));
        mainImageItems.add(new ImageItem(R.drawable._check, "Check", true));

        mainImageItems.add(new ImageItem(R.drawable._water, "Utilities", true));
        mainImageItems.add(new ImageItem(R.drawable._shopping, "Shopping", true));
        mainImageItems.add(new ImageItem(R.drawable._atm, "ATM", true));
        mainImageItems.add(new ImageItem(R.drawable._building, "Taxes", true));
        mainImageItems.add(new ImageItem(R.drawable._suitcase, "Travel", true));

        // incomes
        mainImageItems.add(new ImageItem(R.drawable._cash_income, "Other Income", false));
        mainImageItems.add(new ImageItem(R.drawable._salary, "Salary", false));
        mainImageItems.add(new ImageItem(R.drawable._building, "Tax Refund", false));
        mainImageItems.add(new ImageItem(R.drawable._gift, "Gifts", false));

        return mainImageItems;
    }

    public ArrayList<ImageItem> getArrayListOfSubCategories() {

        ArrayList<ImageItem> subImageItems = new ArrayList<>();

        subImageItems.add(new ImageItem(R.drawable._car, "Car loan expenses", "Car"));
        subImageItems.add(new ImageItem(R.drawable._car, "Fuel", "Car"));
        subImageItems.add(new ImageItem(R.drawable._car, "Miscellaneous", "Car"));
        subImageItems.add(new ImageItem(R.drawable._car, "Parking and Tolls", "Car"));
        subImageItems.add(new ImageItem(R.drawable._car, "Service and Maintenance", "Car"));

        subImageItems.add(new ImageItem(R.drawable._credit_card, "Credit Card", "Debt"));
        subImageItems.add(new ImageItem(R.drawable._debt, "Loan", "Debt"));
        subImageItems.add(new ImageItem(R.drawable._debt, "Other", "Debt"));

        subImageItems.add(new ImageItem(R.drawable._games, "Games", "Entertainment"));
        subImageItems.add(new ImageItem(R.drawable._movies, "Movies", "Entertainment"));
        subImageItems.add(new ImageItem(R.drawable._music, "Music", "Entertainment"));
        subImageItems.add(new ImageItem(R.drawable._tv, "Television", "Entertainment"));

        subImageItems.add(new ImageItem(R.drawable._pacifier, "Child Care", "Family and Personal"));
        subImageItems.add(new ImageItem(R.drawable._gym, "Fitness", "Family and Personal"));
        subImageItems.add(new ImageItem(R.drawable._medical, "Medical", "Family and Personal"));
        subImageItems.add(new ImageItem(R.drawable._dog, "Pet Care", "Family and Personal"));

        subImageItems.add(new ImageItem(R.drawable._repair, "Maintenance and Repair", "Home"));
        subImageItems.add(new ImageItem(R.drawable._house, "Mortgage", "Home"));
        subImageItems.add(new ImageItem(R.drawable._house, "Rent", "Home"));

        subImageItems.add(new ImageItem(R.drawable._grocery_cart, "Food", "Household and Food"));
        subImageItems.add(new ImageItem(R.drawable._household, "Household", "Household and Food"));

        subImageItems.add(new ImageItem(R.drawable._shirt, "Clothing", "Shopping"));
        subImageItems.add(new ImageItem(R.drawable._electronics, "Home", "Shopping"));
        subImageItems.add(new ImageItem(R.drawable._shopping, "Other", "Shopping"));

        subImageItems.add(new ImageItem(R.drawable._plane, "Air Transportation", "Travel"));
        subImageItems.add(new ImageItem(R.drawable._taxi, "Ground Transportation", "Travel"));
        subImageItems.add(new ImageItem(R.drawable._hotel, "Lodging", "Travel"));
        subImageItems.add(new ImageItem(R.drawable._meals, "Dining", "Travel"));
        subImageItems.add(new ImageItem(R.drawable._cocktails, "Cocktails", "Travel"));

        subImageItems.add(new ImageItem(R.drawable._car, "Car", "Insurance"));
        subImageItems.add(new ImageItem(R.drawable._home, "Home", "Insurance"));
        subImageItems.add(new ImageItem(R.drawable._medical_insurance, "Medical", "Insurance"));
        subImageItems.add(new ImageItem(R.drawable._insurance, "Other", "Insurance"));

        subImageItems.add(new ImageItem(R.drawable._television, "Cable", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._cell_phone, "Cell Phone", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._electricity, "Electrical", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._water, "Water", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._natural_gas, "Natural Gas", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._internet, "Internet", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._trash, "Trash", "Utilities"));
        subImageItems.add(new ImageItem(R.drawable._utility, "Miscellaneous", "Utilities"));

        return subImageItems;
    }

    public ArrayList<ImageItem> getExpenseCategories(){
        return db.getExpenseCategories();
    }

    public ArrayList<ImageItem> getIncomeCategories(){
        return db.getIncomeCategories();
    }

    public ArrayList<ImageItem> getSubCategoryList(String name){
        return db.getSubCategoryList(name);
    }
}
