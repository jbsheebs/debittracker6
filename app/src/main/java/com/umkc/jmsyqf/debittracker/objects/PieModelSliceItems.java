package com.umkc.jmsyqf.debittracker.objects;


import java.text.NumberFormat;

public class PieModelSliceItems {

    private int amount;
    private String color;
    private String category;
    private int imageResourceId;

    private NumberFormat nf = NumberFormat.getCurrencyInstance();

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAmount() {
        return nf.format(amount/100.0);
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public PieModelSliceItems(String color, int amount, String category, int imageId) {
        this.color = color;
        this.amount = amount;
        this.category = category;
        this.imageResourceId = imageId;
    }

}
