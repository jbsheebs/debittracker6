package com.umkc.jmsyqf.debittracker.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.umkc.jmsyqf.debittracker.R;

import java.util.ArrayList;

public class DateDialogAdapter extends ArrayAdapter<String>{

    private ArrayList<String> dateOptions;
    private LayoutInflater layoutInflater;
    private Context context;

    public DateDialogAdapter(Context context, ArrayList<String> results) {
        super(context, R.layout.date_options_single_choice, results);
        this.dateOptions = new ArrayList<>();
        this.dateOptions.addAll(results);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public String getItem(int position){
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.date_options_single_choice, parent, false);
            holder = new ViewHolder();
            holder.dateRange = (TextView) convertView.findViewById(R.id.date_option);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        String option = dateOptions.get(position);
        holder.dateRange.setText(option);
        return convertView;
    }

    static class ViewHolder {
        TextView dateRange;
    }
}
