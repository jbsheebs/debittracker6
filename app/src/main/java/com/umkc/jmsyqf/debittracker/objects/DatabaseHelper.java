package com.umkc.jmsyqf.debittracker.objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.umkc.jmsyqf.debittracker.controllers.CategoryHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "accounts.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_ACCOUNTS = "accounts";
    public static final String TABLE_TRANSACTIONS = "transactions";
    public static final String TABLE_CATEGORIES = "categories";
    public static final String TABLE_SUB_CATEGORIES = "sub_categories";

    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_RESOURCE_ID = "resource_id";
    public static final String CATEGORY_TYPE = "type";

    public static final String SUB_CATEGORY_ID = "id";
    public static final String SUB_CATEGORY_NAME = "sub_category_name";
    public static final String SUB_CATEGORY_RESOURCE_ID = "resource_id";
    public static final String SUB_CATEGORY_PARENT_NAME = "parent_category";

    public static final String ACCOUNT_ID = "account_id";
    public static final String ACCOUNT_NAME = "account_name";
    public static final String ACCOUNT_BALANCE = "account_balance";
    public static final String ACCOUNT_CLEARED_BALANCE = "cleared_balance";

    public static final String TRANSACTION_ID = "transaction_id";
    public static final String TRANSACTION_OWNER = "transaction_owner";
    public static final String TRANSACTION_TYPE = "transaction_type";
    public static final String TRANSACTION_CATEGORY = "transaction_category";
    public static final String TRANSACTION_AMOUNT = "transaction_amount";
    public static final String TRANSACTION_MONTH = "month";
    public static final String TRANSACTION_DAY = "day";
    public static final String TRANSACTION_YEAR = "year";
    public static final String TRANSACTION_HOUR = "hour";
    public static final String TRANSACTION_MINUTE = "minute";
    public static final String TRANSACTION_SECOND = "second";
    public static final String TRANSACTION_DESCRIPTION = "transaction_desc";
    public static final String TRANSACTION_RESOURCE_ID = "resource_id";
    public static final String TRANSACTION_IS_CLEARED = "cleared";
    public static final String TRANSACTION_RUNNING_BALANCE = "running_balance";


    public static final String DATABASE_CREATE_ACCOUNTS = "CREATE TABLE " +
            TABLE_ACCOUNTS + " (" + ACCOUNT_ID + " INT, " +
            ACCOUNT_NAME + " TEXT PRIMARY KEY, " + ACCOUNT_BALANCE + " INT, " + ACCOUNT_CLEARED_BALANCE + " INT);";

    public static final String DATABASE_CREATE_TRANSACTIONS = "create table " +
            TABLE_TRANSACTIONS + " (" + TRANSACTION_ID + " integer primary key autoincrement, " +
            TRANSACTION_OWNER + " text," + TRANSACTION_AMOUNT + " int, " +
            TRANSACTION_TYPE + " text, " + TRANSACTION_CATEGORY + " text, " + TRANSACTION_DESCRIPTION + " text, " +
            TRANSACTION_MONTH + " int, " + TRANSACTION_DAY + " int, " + TRANSACTION_YEAR + " int, " +
            TRANSACTION_HOUR + " int, " + TRANSACTION_MINUTE + " int, " + TRANSACTION_SECOND + " int, " +
            TRANSACTION_RESOURCE_ID + " int, " + TRANSACTION_IS_CLEARED + " boolean, " +
            TRANSACTION_RUNNING_BALANCE + " int);";

    public static final String DATABASE_CREATE_CATEGORIES = "create table " + TABLE_CATEGORIES + " (" +
            CATEGORY_ID + " integer, " + CATEGORY_NAME + " text primary key, " +
            CATEGORY_RESOURCE_ID + " int, " + CATEGORY_TYPE + " boolean);";

    public static final String DATABASE_CREATE_SUB_CATEGORIES = "create table " + TABLE_SUB_CATEGORIES + " (" +
            SUB_CATEGORY_ID + " integer, " + SUB_CATEGORY_NAME + " text, " +
            SUB_CATEGORY_RESOURCE_ID + " int, " + SUB_CATEGORY_PARENT_NAME + " text, PRIMARY KEY (" +
            SUB_CATEGORY_NAME + ", " + SUB_CATEGORY_PARENT_NAME + "));";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_ACCOUNTS);
        db.execSQL(DATABASE_CREATE_TRANSACTIONS);
        db.execSQL(DATABASE_CREATE_CATEGORIES);
        db.execSQL(DATABASE_CREATE_SUB_CATEGORIES);

        // insert categories only once on create
        CategoryHelper categoryHelper = new CategoryHelper();
        ArrayList<ImageItem> mainCategoryList = categoryHelper.getArrayListOfMainCategories();
        addCategoriesToDB(mainCategoryList, db);

        // insert sub categories only once on create
        CategoryHelper categoryHelper1 = new CategoryHelper();
        ArrayList<ImageItem> subCategoryList = categoryHelper1.getArrayListOfSubCategories();
        addSubCategoriesToDB(subCategoryList, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB_CATEGORIES);
        onCreate(db);
    }

    public void addCategoriesToDB(ArrayList<ImageItem> items, SQLiteDatabase db){
        for (int i = 0; i < items.size(); i++){
            ContentValues values = new ContentValues();
            values.put(CATEGORY_NAME, items.get(i).getCategoryName());
            values.put(CATEGORY_RESOURCE_ID, items.get(i).getResourceId());
            values.put(CATEGORY_TYPE, items.get(i).isExpenseType());
            db.insert(TABLE_CATEGORIES, null, values);
        }
    }

    public void addSubCategoriesToDB(ArrayList<ImageItem> items, SQLiteDatabase db) {
        for (int i = 0; i < items.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(SUB_CATEGORY_NAME, items.get(i).getCategoryName());
            values.put(SUB_CATEGORY_RESOURCE_ID, items.get(i).getResourceId());
            values.put(SUB_CATEGORY_PARENT_NAME, items.get(i).getParentCategoryName());
            db.insert(TABLE_SUB_CATEGORIES, null, values);

        }
    }

    public ArrayList<ImageItem> getSubCategoryList(String name) {

        ArrayList<ImageItem> subCategoryList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + SUB_CATEGORY_NAME + ", " + SUB_CATEGORY_RESOURCE_ID + " FROM " +
                TABLE_SUB_CATEGORIES + " WHERE " + SUB_CATEGORY_PARENT_NAME + " ='" + name + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ImageItem imageItem = new ImageItem();
                imageItem.setCategoryName(cursor.getString(0));
                imageItem.setResourceId(cursor.getInt(1));

                subCategoryList.add(imageItem);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return subCategoryList;
    }

    public ArrayList<ImageItem> getExpenseCategories(){
        ArrayList<ImageItem> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT " + CATEGORY_NAME + ", " + CATEGORY_RESOURCE_ID + " FROM " +
                TABLE_CATEGORIES + " WHERE " + CATEGORY_TYPE + " > 0";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                ImageItem item = new ImageItem();
                item.setCategoryName(cursor.getString(0));
                item.setResourceId(cursor.getInt(1));

                list.add(item);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<ImageItem> getIncomeCategories(){
        ArrayList<ImageItem> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT " + CATEGORY_NAME + ", " + CATEGORY_RESOURCE_ID + " FROM " +
                TABLE_CATEGORIES + " WHERE " + CATEGORY_TYPE + " < 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                ImageItem item = new ImageItem();
                item.setCategoryName(cursor.getString(0));
                item.setResourceId(cursor.getInt(1));

                list.add(item);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return list;
    }

    public void updateRunningBalances(Transaction transaction, int currentBalance){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TRANSACTION_RUNNING_BALANCE, currentBalance);
        db.update(TABLE_TRANSACTIONS, values, TRANSACTION_ID + "='" + transaction.getTransID() + "'", null);
        db.close();
    }

    public ArrayList<String> getAccountNames(){
        ArrayList<String> accountList = new ArrayList<>();
        String selectQuery = "SELECT " + ACCOUNT_NAME + " FROM " + TABLE_ACCOUNTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(0);
                // Adding account to list
                accountList.add(name);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        // return contact list
        return accountList;
    }

    public void updateAccountBalance(String name, int balance){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ACCOUNT_BALANCE, balance);
        db.update(TABLE_ACCOUNTS, values, ACCOUNT_NAME + "='" + name + "'", null);
        db.close();
    }

    public void updateAccountClearedBalance(String name, int clearedBalance){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ACCOUNT_CLEARED_BALANCE, clearedBalance);
        db.update(TABLE_ACCOUNTS, values, ACCOUNT_NAME + "='" + name + "'", null);
        db.close();
    }

    public void updateTransaction(Transaction transaction) throws InvalidParameterException {

        // update transaction to DB
        // throws exception if transaction is null
        if (transaction == null){
            throw new InvalidParameterException("Invalid transaction.");
        } else {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(TRANSACTION_OWNER, transaction.getOwnerName());
            values.put(TRANSACTION_MONTH, transaction.getTransMonth());
            values.put(TRANSACTION_DAY, transaction.getTransDay());
            values.put(TRANSACTION_YEAR, transaction.getTransYear());
            values.put(TRANSACTION_HOUR, transaction.getTransHour());
            values.put(TRANSACTION_MINUTE, transaction.getTransMinute());
            values.put(TRANSACTION_SECOND, transaction.getTransSecond());
            values.put(TRANSACTION_TYPE, transaction.getTransType());
            values.put(TRANSACTION_CATEGORY, transaction.getTransCategory());
            values.put(TRANSACTION_DESCRIPTION, transaction.getTransDescription());
            values.put(TRANSACTION_AMOUNT, transaction.getTransAmount());
            values.put(TRANSACTION_RESOURCE_ID, transaction.getImageResourceId());
            values.put(TRANSACTION_IS_CLEARED, transaction.isCleared());
            db.update(TABLE_TRANSACTIONS, values, TRANSACTION_ID + "='" + transaction.getTransID() + "'", null);
            db.close();
        }
    }

    public void addAccount(Account account){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ACCOUNT_NAME, account.getAccountName());
        values.put(ACCOUNT_BALANCE, account.getAccountBalance());
        values.put(ACCOUNT_CLEARED_BALANCE, account.getAccountClearedBalance());
        // throw exception if account name already exists
        db.insertOrThrow(TABLE_ACCOUNTS, null, values);
        db.close();

    }

    public void addTransaction(Transaction transaction) throws InvalidParameterException {

        // add transaction to DB
        // throws exception if transaction is null
        if (transaction == null) {
            throw new InvalidParameterException("Invalid transaction.");
        } else {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(TRANSACTION_OWNER, transaction.getOwnerName());
            values.put(TRANSACTION_MONTH, transaction.getTransMonth());
            values.put(TRANSACTION_DAY, transaction.getTransDay());
            values.put(TRANSACTION_YEAR, transaction.getTransYear());
            values.put(TRANSACTION_HOUR, transaction.getTransHour());
            values.put(TRANSACTION_MINUTE, transaction.getTransMinute());
            values.put(TRANSACTION_SECOND, transaction.getTransSecond());
            values.put(TRANSACTION_TYPE, transaction.getTransType());
            values.put(TRANSACTION_CATEGORY, transaction.getTransCategory());
            values.put(TRANSACTION_DESCRIPTION, transaction.getTransDescription());
            values.put(TRANSACTION_AMOUNT, transaction.getTransAmount());
            values.put(TRANSACTION_RESOURCE_ID, transaction.getImageResourceId());
            values.put(TRANSACTION_IS_CLEARED, transaction.isCleared());
            values.put(TRANSACTION_RUNNING_BALANCE, transaction.getTotalBalance());
            db.insert(TABLE_TRANSACTIONS, null, values);
            db.close();
        }

    }

    public int getTotalBalancesFromTransactionsForAllAccounts(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE transaction_type = 'Expense'", null);
        int intExp = 0;
        if (cursor.moveToFirst()){
            intExp = cursor.getInt(0);
        }
        cursor.close();

        int intInc = 0;
        Cursor cursor2 = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE transaction_type = 'Income'", null);
        if (cursor2.moveToFirst()) {
            intInc = cursor2.getInt(0);
        }
        cursor2.close();
        db.close();
        return intInc - intExp;
    }

    public int getTotalBalanceFromTransactions(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE transaction_owner = '" +
                name + "' AND transaction_type = 'Expense'", null);
        int intExp = 0;
        if (cursor.moveToFirst()){
            intExp = cursor.getInt(0);
        }
        cursor.close();

        int intInc = 0;
        Cursor cursor2 = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE transaction_owner = '" +
                name + "' AND transaction_type = 'Income'", null);
        if (cursor2.moveToFirst()) {
            intInc = cursor2.getInt(0);
        }

        cursor2.close();
        db.close();
        return intInc - intExp;
    }

    public int getTotalAmountOfTransactionsForTransactionType(boolean isExpense){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery;
        if (isExpense){
            selectQuery = "SELECT SUM(transaction_amount) FROM " + TABLE_TRANSACTIONS + " where " + TRANSACTION_TYPE + " = 'Expense'";
        } else {
            selectQuery = "select SUM(transaction_amount) FROM " + TABLE_TRANSACTIONS + " where " + TRANSACTION_TYPE + " = 'Income'";
        }
        Cursor cursor = db.rawQuery(selectQuery, null);
        int intTotalTransactionSum = 0;
        if (cursor.moveToFirst()){
            intTotalTransactionSum = cursor.getInt(0);
        }
        cursor.close();
        db.close();
        return intTotalTransactionSum;
    }

    public PieChartCategoryItems getCategoryAmountAndImageId(String category){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select SUM(transaction_amount) FROM " + TABLE_TRANSACTIONS +
                " where " + TRANSACTION_CATEGORY + " = '" + category + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        int categoryTotalAmount = 0;
        if (cursor.moveToFirst()){
            categoryTotalAmount = cursor.getInt(0);
        }
        cursor.close();
        //return categoryTotalAmount;
        String selectQuery1 = "select " + TRANSACTION_RESOURCE_ID + " FROM " + TABLE_TRANSACTIONS +
                " where " + TRANSACTION_CATEGORY + " ='" + category + "'";
        Cursor cursor1 = db.rawQuery(selectQuery1, null);
        int categoryImageId = 0;
        if (cursor1.moveToFirst()){
            categoryImageId = cursor1.getInt(0);
        }
        cursor1.close();
        db.close();
        return new PieChartCategoryItems(category, categoryTotalAmount, categoryImageId);
    }

    public int getClearedBalancesFromTransactionsForAllAccounts(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE " +
                "transaction_type = 'Expense' AND cleared > 0", null);
        int expenseAndCleared = 0;
        if (cursor.moveToFirst()){
            expenseAndCleared = cursor.getInt(0);
        }
        cursor.close();

        Cursor cursor1 = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE " +
                "transaction_type = 'Income' AND cleared > 0", null);
        int incomeAndCleared = 0;
        if (cursor1.moveToFirst()){
            incomeAndCleared = cursor1.getInt(0);
        }
        cursor1.close();
        db.close();
        return incomeAndCleared - expenseAndCleared;
    }

    public int getClearedBalanceFromTransactions(String name){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE transaction_owner = '" +
                name + "' AND transaction_type = 'Expense' AND cleared > 0", null);
        int expenseAndCleared = 0;
        if (cursor.moveToFirst()){
            expenseAndCleared = cursor.getInt(0);
        }
        cursor.close();

        Cursor cursor1 = db.rawQuery("SELECT SUM(transaction_amount) FROM transactions WHERE transaction_owner = '" +
                name + "' AND transaction_type = 'Income' AND cleared > 0", null);
        int incomeAndCleared = 0;
        if (cursor1.moveToFirst()){
            incomeAndCleared = cursor1.getInt(0);
        }
        cursor1.close();
        db.close();
        return incomeAndCleared - expenseAndCleared;
    }

    public ArrayList<Transaction> getAllTransactionBasedOnIncomeOrExpense(boolean isExpense){
        ArrayList<Transaction> transactionList = new ArrayList<>();
        String selectQuery;
        if (isExpense){
            selectQuery = "select * from " + TABLE_TRANSACTIONS + " where " + TRANSACTION_TYPE + " = 'Expense'";
        } else {
            selectQuery = "select * from " + TABLE_TRANSACTIONS + " where " + TRANSACTION_TYPE + " = 'Income'";
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do {
                Transaction transaction = new Transaction();
                transaction.setTransID(cursor.getInt(0));
                transaction.setOwnerName(cursor.getString(1));
                transaction.setTransAmount(cursor.getInt(2));
                transaction.setTransType(cursor.getString(3));
                transaction.setTransCategory(cursor.getString(4));
                transaction.setTransDescription(cursor.getString(5));
                transaction.setTransMonth(cursor.getInt(6));
                transaction.setTransDay(cursor.getInt(7));
                transaction.setTransYear(cursor.getInt(8));
                transaction.setTransHour(cursor.getInt(9));
                transaction.setTransMinute(cursor.getInt(10));
                transaction.setTransSecond(cursor.getInt(11));
                transaction.setImageResourceId(cursor.getInt(12));
                transaction.setCleared(cursor.getInt(13)>0);

                transactionList.add(transaction);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return transactionList;

    }


    // get specific transactions
    public ArrayList<Transaction> getAllTransactionsByOwner(String name) {
        ArrayList<Transaction> transactionList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_TRANSACTIONS + " WHERE " + TRANSACTION_OWNER + " = " + "'" + name + "'" +
               " ORDER BY " + TRANSACTION_YEAR + " desc," + TRANSACTION_MONTH + " desc," + TRANSACTION_DAY + " desc," + TRANSACTION_HOUR +
                " desc," + TRANSACTION_MINUTE + " desc," + TRANSACTION_SECOND + " desc";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do {
                Transaction transaction = new Transaction();
                transaction.setTransID(cursor.getInt(0));
                transaction.setOwnerName(cursor.getString(1));
                transaction.setTransAmount(cursor.getInt(2));
                transaction.setTransType(cursor.getString(3));
                transaction.setTransCategory(cursor.getString(4));
                transaction.setTransDescription(cursor.getString(5));
                transaction.setTransMonth(cursor.getInt(6));
                transaction.setTransDay(cursor.getInt(7));
                transaction.setTransYear(cursor.getInt(8));
                transaction.setTransHour(cursor.getInt(9));
                transaction.setTransMinute(cursor.getInt(10));
                transaction.setTransSecond(cursor.getInt(11));
                transaction.setImageResourceId(cursor.getInt(12));
                transaction.setCleared(cursor.getInt(13)>0);

                transactionList.add(transaction);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return transactionList;
    }

    // get all transactions with running balances for ALL ACCOUNTS transactions view
    public ArrayList<Transaction> getAllTransactions() {
        ArrayList<Transaction> transactionList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_TRANSACTIONS + " ORDER BY " + TRANSACTION_YEAR + " desc," +
                TRANSACTION_MONTH + " desc," + TRANSACTION_DAY + " desc," + TRANSACTION_HOUR +
                " desc," + TRANSACTION_MINUTE + " desc," + TRANSACTION_SECOND + " desc";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do {
                Transaction transaction = new Transaction();
                transaction.setTransID(cursor.getInt(0));
                transaction.setOwnerName(cursor.getString(1));
                transaction.setTransAmount(cursor.getInt(2));
                transaction.setTransType(cursor.getString(3));
                transaction.setTransCategory(cursor.getString(4));
                transaction.setTransDescription(cursor.getString(5));
                transaction.setTransMonth(cursor.getInt(6));
                transaction.setTransDay(cursor.getInt(7));
                transaction.setTransYear(cursor.getInt(8));
                transaction.setTransHour(cursor.getInt(9));
                transaction.setTransMinute(cursor.getInt(10));
                transaction.setTransSecond(cursor.getInt(11));
                transaction.setImageResourceId(cursor.getInt(12));
                transaction.setCleared(cursor.getInt(13)>0);
                transaction.setTotalBalance(cursor.getInt(14));

                transactionList.add(transaction);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return transactionList;
    }

    // get all accounts
    public ArrayList<Account> getAllAccounts() {
        ArrayList<Account> accountList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ACCOUNTS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Account account = new Account();
                account.setAccountID(cursor.getInt(0));
                account.setAccountName(cursor.getString(1));
                account.setAccountTotalBalance(cursor.getInt(2));
                account.setAccountClearedBalance(cursor.getInt(3));
                // Adding account to list
                accountList.add(account);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        // return contact list
        return accountList;
    }

    // Deleting single account
    // Must delete all transactions associated with the account
    public void deleteAccount(Account account) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TRANSACTIONS, TRANSACTION_OWNER + " ='" + account.getAccountName() + "'", null);
        db.delete(TABLE_ACCOUNTS, ACCOUNT_NAME + " ='" + account.getAccountName() + "'", null);
        db.close();
    }

    public void deleteTransaction(Transaction transaction){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TRANSACTIONS, TRANSACTION_ID + "='" + transaction.getTransID() + "'", null);
        db.close();
    }
}
