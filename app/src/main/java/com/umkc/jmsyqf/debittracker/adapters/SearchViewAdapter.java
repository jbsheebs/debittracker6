package com.umkc.jmsyqf.debittracker.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.umkc.jmsyqf.debittracker.R;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

import junit.framework.Assert;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;

public class SearchViewAdapter extends ArrayAdapter<Transaction> implements Filterable {

    private NumberFormat nf = NumberFormat.getCurrencyInstance();

    private ArrayList<Transaction> originalTransactionArrayList;
    private ArrayList<Transaction> filteredTransactionArrayList;
    private TransactionFilter filter;

    private LayoutInflater layoutInflater;
    private static final int NO_IMAGE = 0;
    private static final int WITH_IMAGE = 1;

    private Context context;

    public SearchViewAdapter(Context context, ArrayList<Transaction> results) {
        super(context, R.layout.searchable_transaction_row_layout, results);
        this.context = context;
        this.originalTransactionArrayList = new ArrayList<>();
        originalTransactionArrayList.addAll(results);
        this.filteredTransactionArrayList = new ArrayList<>();
        filteredTransactionArrayList.addAll(originalTransactionArrayList);
        layoutInflater = LayoutInflater.from(context);
        getFilter();
    }

    @Override
    public Filter getFilter() {
        if (filter == null){
            filter = new TransactionFilter();
        }
        return filter;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type;
        if (filteredTransactionArrayList.get(position).getTransCategory().equals("OPENING BALANCE")
            || filteredTransactionArrayList.get(position).getImageResourceId() == 0){
            type = NO_IMAGE;
        } else {
            type = WITH_IMAGE;
        }
        return type;
    }

    @Override
    public Transaction getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        Transaction transaction = filteredTransactionArrayList.get(position);
        int type = getItemViewType(position);

        if (convertView == null) {

            if (type == WITH_IMAGE) {
                convertView = layoutInflater.inflate(R.layout.searchable_transaction_row_layout, parent, false);
            } else {
                convertView = layoutInflater.inflate(R.layout.searchable_transaction_row_layout_no_image, parent, false);
            }
            viewHolder = new ViewHolder();
            viewHolder.type = (TextView) convertView.findViewById(R.id.tfTransType);
            viewHolder.date = (TextView) convertView.findViewById(R.id.tfDate);
            viewHolder.transAmount = (TextView) convertView.findViewById(R.id.tfTransAmount);
            viewHolder.balance = (TextView) convertView.findViewById(R.id.tfTotalBalance);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.list_image);
            viewHolder.description = (TextView) convertView.findViewById(R.id.textViewDescription);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = ((ViewHolder) convertView.getTag());
        }

        if (transaction.isExpenseType()) {
            viewHolder.transAmount.setTextColor(Color.RED);
        } else {
            viewHolder.transAmount.setTextColor(Color.parseColor("#008000"));
        }

        viewHolder.description.setText(transaction.getDescriptionWithParens());

        if (type == WITH_IMAGE) {
            // assert image resource id is not null
            Assert.assertNotNull(transaction.getImageResourceId());
            viewHolder.image.setImageResource(transaction.getImageResourceId());
        }
        viewHolder.type.setText(transaction.getTransCategory());
        viewHolder.date.setText(transaction.getDateAndTimeString());
        viewHolder.transAmount.setText(nf.format(transaction.getTransAmount() / 100.0));
        viewHolder.balance.setText(nf.format(transaction.getTotalBalance() / 100.0));

        return convertView;
    }

    static class ViewHolder {
        TextView type;
        TextView date;
        TextView transAmount;
        TextView balance;
        TextView description;
        ImageView image;
    }

    private class TransactionFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.toString().length() > 0) {

                ArrayList<Transaction> filteredItems = new ArrayList<>();

                for (int i = 0; i < originalTransactionArrayList.size(); i++) {

                    Transaction transaction = originalTransactionArrayList.get(i);

                    // should find a more efficient way around this...
                    int transAmount = transaction.getTransAmount();
                    String amountString = String.valueOf(transAmount);
                    BigInteger bi = new BigInteger(amountString);
                    BigDecimal bd = new BigDecimal(bi, 2);
                    String finalString = String.valueOf(bd);

                    if (transaction.getTransCategory().toLowerCase().contains(constraint)
                            || transaction.getTransDescription().toLowerCase().contains(constraint)
                            || finalString.toLowerCase().contains(constraint)){

                        filteredItems.add(transaction);
                    }
                }

                results.values = filteredItems;
                results.count = filteredItems.size();

            } else {

                synchronized(this){
                    results.values = originalTransactionArrayList;
                    results.count = originalTransactionArrayList.size();
                }
            }
            return results;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredTransactionArrayList = (ArrayList<Transaction>)results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0; i < filteredTransactionArrayList.size(); i++){
                add(filteredTransactionArrayList.get(i));
            }
            notifyDataSetInvalidated();
        }
    }





}
