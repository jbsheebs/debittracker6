package com.umkc.jmsyqf.debittracker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.umkc.jmsyqf.debittracker.R;

import java.util.ArrayList;

import com.umkc.jmsyqf.debittracker.objects.ImageItem;

public class GridViewAdapter extends ArrayAdapter<ImageItem> {

    //private Context context;
    private ArrayList<ImageItem> categories;
    private LayoutInflater layoutInflater;

    public GridViewAdapter(Context context, ArrayList<ImageItem> categories) {
        super(context, R.layout.single_grid_element, categories);
        //this.context = context;
        this.categories = categories;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder;
        if (convertView == null){

            convertView = layoutInflater.inflate(R.layout.single_grid_element, parent, false);
            holder = new ViewHolder();

            holder.categoryName = (TextView)convertView.findViewById(R.id.grid_text);
            holder.imageIcon = (ImageView)convertView.findViewById(R.id.grid_image);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        ImageItem item = categories.get(position);
        holder.categoryName.setText(item.getCategoryName());
        holder.imageIcon.setImageResource(item.getResourceId());
        return convertView;
    }


    static class ViewHolder {
        TextView categoryName;
        ImageView imageIcon;
    }
}
