package com.umkc.jmsyqf.debittracker.objects;

public class PieChartCategoryItems {

    public String category;
    public int amount;
    public int imageResourceId;

    public PieChartCategoryItems(String category, int amount, int imageResourceId){
        this.category = category;
        this.amount = amount;
        this.imageResourceId = imageResourceId;
    }

    public PieChartCategoryItems() {

    }
}
