package com.umkc.jmsyqf.debittracker.controllers;

import android.app.Activity;
import android.content.Context;

import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

import java.util.ArrayList;

public class DatabaseGateway extends Activity {

    private String name;
    private Context context;

    public DatabaseGateway(Context context, String accountName){
        this.context = context;
        name = accountName;
    }

    public DatabaseGateway(Context context){
        this.context = context;
    }

    public ArrayList<Transaction> setAllBalancesForAccount(){
        ArrayList<Transaction> list;
        int nextTotalBalance;
        int expenseAmount;
        int incomeAmount;
        int tb;
        DatabaseHelper db = new DatabaseHelper(context);
        list = db.getAllTransactionsByOwner(name);
        int totalBalance = db.getTotalBalanceFromTransactions(name);
        db.updateAccountBalance(name, totalBalance);
        if (!list.isEmpty()) {
            list.get(0).setTotalBalance(totalBalance);

            db.updateRunningBalances(list.get(0), totalBalance);

            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(i).isExpenseType()) {
                    expenseAmount = list.get(i).getTransAmount();
                    tb = list.get(i).getTotalBalance();
                    nextTotalBalance = tb + expenseAmount;
                    list.get(i + 1).setTotalBalance(nextTotalBalance);

                    db.updateRunningBalances(list.get(i + 1), nextTotalBalance);

                } else {
                    incomeAmount = list.get(i).getTransAmount();
                    tb = list.get(i).getTotalBalance();
                    nextTotalBalance = tb - incomeAmount;
                    list.get(i + 1).setTotalBalance(nextTotalBalance);

                    db.updateRunningBalances(list.get(i + 1), nextTotalBalance);
                }
            }
        }
        return list;
    }

    public void updateAllBalances() {
        DatabaseHelper db = new DatabaseHelper(context);
        ArrayList<String> names = db.getAccountNames();
        for (int i = 0; i < names.size(); i++){
            this.name = names.get(i);
            setAllBalancesForAccount();
        }
    }
}
