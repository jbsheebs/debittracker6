package com.umkc.jmsyqf.debittracker.objects;

import java.io.Serializable;

public class ImageItem implements Serializable {

    private boolean isExpenseType;
    private int imageItemId;
    private int resourceId;
    private String name;
    private String parentCategory;

    public ImageItem(){}

    public ImageItem(int resourceId, String title) {
        super();
        this.resourceId = resourceId;
        this.name = title;
        this.parentCategory = null;
    }

    public ImageItem(int resourceId, String title, boolean isExpenseType) {
        super();
        this.resourceId = resourceId;
        this.name = title;
        this.isExpenseType = isExpenseType;
        this.parentCategory = null;
    }

    public ImageItem(int resourceId, String name, String parentName){
        super();
        this.resourceId = resourceId;
        this.name = name;
        this.parentCategory = parentName;
    }

    public ImageItem(int itemId, int resourceId, String name, String parentName){
        super();
        this.imageItemId = itemId;
        this.resourceId = resourceId;
        this.name = name;
        this.parentCategory = parentName;
    }

    public boolean isExpenseType() {
        return isExpenseType;
    }

    public void setExpenseType(boolean isExpenseType) {
        this.isExpenseType = isExpenseType;
    }

    public int getImageItemId() {
        return imageItemId;
    }

    public void setImageItemId(int imageItemId) {
        this.imageItemId = imageItemId;
    }

    public String getParentCategoryName() {
        return parentCategory;
    }

    public void setParentCategoryName(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getCategoryName() {
        return name;
    }

    public void setCategoryName(String name) {
        this.name = name;
    }
}