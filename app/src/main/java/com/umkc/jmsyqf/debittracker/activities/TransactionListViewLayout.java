package com.umkc.jmsyqf.debittracker.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;

import com.umkc.jmsyqf.debittracker.adapters.DateDialogAdapter;
import com.umkc.jmsyqf.debittracker.adapters.TransactionAdapter;
import com.umkc.jmsyqf.debittracker.controllers.DateTimeHelper;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.controllers.DatabaseGateway;
import com.umkc.jmsyqf.debittracker.objects.Transaction;
import com.umkc.jmsyqf.debittracker.R;

public class TransactionListViewLayout extends ActionBarActivity {

    private String name;
    private ArrayList<Transaction> transactionList = new ArrayList<>();
    private ListView listView;
    private DatabaseHelper db;
    private TextView accountBalance;
    private TextView clearedBalance;
    private NumberFormat nf;
    private final String TRANSACTION_LAYOUT_OWNER_NAME = "TRANSACTION_LAYOUT_NAME";
    private Context context = this;
    private Dialog dialog;
    private int totalBalance;
    private int totalClearedBalance;
    private DateTimeHelper dateTimeHelper;

    private static String dateFilterMode;
    //public static final String DATE_MODE = "date_mode";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_activity_list_view);

        db = new DatabaseHelper(this);
        nf = NumberFormat.getCurrencyInstance();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_add_transaction);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();
        name = intent.getExtras().getString("NAME");
        TextView accountName = (TextView)findViewById(R.id.trans_account_name);
        accountName.setText(name);

        dateFilterMode = "MONTH";

        dateTimeHelper = new DateTimeHelper(dateFilterMode);
        String monthYearString = dateTimeHelper.getMonthAndYearString();
        final TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(monthYearString);

        listView = (ListView)findViewById(R.id.transaction_list_view);
        accountBalance = (TextView)findViewById(R.id.trans_account_balance);
        clearedBalance = (TextView)findViewById(R.id.trans_cleared_balance);

        setTransactionList();

        final ArrayList<String> dateOptionsList = dateTimeHelper.getDateTimeOptions();

        final Dialog dateDialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.date_options_list_view, null);
        final ListView dateListView = (ListView)view.findViewById(R.id.custom_list_view);

        dateListView.setAdapter(new DateDialogAdapter(this, dateOptionsList));
        dateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dateDialog.setContentView(view);
        WindowManager.LayoutParams params = dateDialog.getWindow().getAttributes();
        params.gravity = Gravity.TOP | Gravity.CENTER;

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog.show();
            }
        });

        dateListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dateFilterMode = dateOptionsList.get(position);
                dateDialog.dismiss();
                DateTimeHelper dateTimeHelper = new DateTimeHelper();
                title.setText(dateTimeHelper.getCurrentDateRange(dateFilterMode));
                setTransactionList();
            }
        });

        ImageView leftArrow = (ImageView)findViewById(R.id.left_arrow);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title.setText(dateTimeHelper.getPreviousDateRange(dateFilterMode));
                setTransactionList();
            }
        });

        ImageView rightArrow = (ImageView)findViewById(R.id.right_arrow);
        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title.setText(dateTimeHelper.getNextDateRange(dateFilterMode));
                setTransactionList();
            }
        });

        accountBalance.setText(nf.format(totalBalance /100.0));
        clearedBalance.setText(nf.format(totalClearedBalance /100.0));

        // set on item click listener and go to editTransaction activity
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setAdapter(new TransactionAdapter(context, transactionList));
                Transaction currentTransaction = transactionList.get(position);
                if (currentTransaction.getTransCategory().equals("OPENING BALANCE")){
                    editOpeningTransaction(currentTransaction);
                } else {
                    editTransaction(currentTransaction);
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                if (!transactionList.get(position).getTransCategory().equals("OPENING BALANCE")) {
                    dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_delete_transaction);
                    dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.water_background_verification));

                    dialog.findViewById(R.id.llDelete).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();

                            final Dialog deleteDialog = new Dialog(context);
                            deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            deleteDialog.setContentView(R.layout.dialog_confirm_delete_transaction);
                            deleteDialog.setCancelable(false);
                            deleteDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.water_dialog_border));
                            Button confirmButton = (Button)deleteDialog.findViewById(R.id.btnDelete);
                            Button cancelButton = (Button)deleteDialog.findViewById(R.id.btnCancel);

                            confirmButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    deleteDialog.dismiss();

                                    listView.setAdapter(new TransactionAdapter(context, transactionList));

                                    // get information about deleted transaction
                                    Transaction deletedTransaction = transactionList.get(position);

                                    // delete transaction from DB
                                    db.deleteTransaction(deletedTransaction);

                                    setTransactionList();

                                    accountBalance.setText(nf.format(totalBalance / 100.0));
                                    clearedBalance.setText(nf.format(totalClearedBalance / 100.0));

                                    CharSequence message2 = "Deleting transaction " + deletedTransaction.getTransCategory();
                                    Context context2 = getApplicationContext();
                                    int duration2 = Toast.LENGTH_SHORT;
                                    Toast toast2 = Toast.makeText(context2, message2, duration2);
                                    toast2.show();
                                }
                            });

                            cancelButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    deleteDialog.dismiss();
                                }
                            });
                            deleteDialog.show();
                        }
                    });
                    dialog.show();
                }
                return true;
            }
        });

        // add transaction button in action bar
        TextView addTransactionIcon = (TextView) findViewById(R.id.add_transaction);
        addTransactionIcon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                addTransaction();
            }
        });
    }

    public void setTransactionList(){

        DatabaseGateway databaseGateway;

        if (name.equals("ALL ACCOUNTS")){

            totalBalance = db.getTotalBalancesFromTransactionsForAllAccounts();
            totalClearedBalance = db.getClearedBalancesFromTransactionsForAllAccounts();
            databaseGateway = new DatabaseGateway(this);
            databaseGateway.updateAllBalances();
            transactionList = dateTimeHelper.getFilteredTransactionListByDate(db.getAllTransactions(), dateFilterMode);

        } else {

            totalBalance = db.getTotalBalanceFromTransactions(name);
            totalClearedBalance = db.getClearedBalanceFromTransactions(name);
            databaseGateway = new DatabaseGateway(this, name);
            transactionList = dateTimeHelper.getFilteredTransactionListByDate(databaseGateway.setAllBalancesForAccount(), dateFilterMode);
        }

        listView.setAdapter(new TransactionAdapter(this, transactionList));
    }

    public void editTransaction(Transaction currentTransaction){
        Intent i = new Intent(TransactionListViewLayout.this, NewEditTransactionForm.class);
        i.putExtra("CURRENT_TRANSACTION", currentTransaction);
        i.putExtra(TRANSACTION_LAYOUT_OWNER_NAME, name);
        startActivityForResult(i, 1);
    }

    public void editOpeningTransaction(Transaction transaction){
        Intent i = new Intent(TransactionListViewLayout.this, EditOpeningBalanceForm.class);
        i.putExtra("CURRENT_TRANSACTION", transaction);
        i.putExtra(TRANSACTION_LAYOUT_OWNER_NAME, name);
        startActivityForResult(i, 1);
    }

    // add new transaction
    public void addTransaction(){
        Intent i = new Intent(TransactionListViewLayout.this, NewEditTransactionForm.class);
        i.putExtra(TRANSACTION_LAYOUT_OWNER_NAME, name);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // on add or edit transaction
        if (resultCode == RESULT_OK && requestCode == 1) {

            name = data.getStringExtra(TRANSACTION_LAYOUT_OWNER_NAME);

            setTransactionList();

            accountBalance.setText(nf.format(totalBalance / 100.0));
            clearedBalance.setText(nf.format(totalClearedBalance / 100.0));
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    public void setActionBar() {
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.transaction_action_bar);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//    }

}