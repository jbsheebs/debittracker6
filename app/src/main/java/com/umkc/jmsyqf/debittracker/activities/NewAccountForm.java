package com.umkc.jmsyqf.debittracker.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.Account;
import com.umkc.jmsyqf.debittracker.objects.InvalidParameterException;
import com.umkc.jmsyqf.debittracker.objects.Transaction;
import com.umkc.jmsyqf.debittracker.R;

public class NewAccountForm extends ActionBarActivity {

    private Button startingBalanceButton;
    private DatabaseHelper db;
    private int finalResultInt;
    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//        Transition fade = new Fade();
//        fade.excludeTarget(android.R.id.statusBarBackground, true);
//        fade.excludeTarget(android.R.id.navigationBarBackground, true);
//        fade.excludeTarget(R.id.main_toolbar_top, true);
//        getWindow().setExitTransition(fade);
//        getWindow().setEnterTransition(fade);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_account_view);
        context = this;

        //setActionBar();
        Toolbar toolbar = (Toolbar)findViewById(R.id.add_account_toolbar_top);
        setSupportActionBar(toolbar);
        db = new DatabaseHelper(this);
        final EditText accountName = (EditText)findViewById(R.id.txtAcctName);
        startingBalanceButton = (Button)findViewById(R.id.btnStartingBalance);
        startingBalanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewAccountForm.this, NumberPadForm.class);
                i.putExtra("VALUE", 0);
                startActivityForResult(i, 1);
            }
        });

        // bug here for button
        int resultBalanceInt = 0;
        startingBalanceButton.setText(nf.format(resultBalanceInt));
        finalResultInt = resultBalanceInt;

        Button saveStartingInformation = (Button)findViewById(R.id.btnSave);
        saveStartingInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String savedAccountName = accountName.getText().toString();
                final Account account = new Account(savedAccountName, finalResultInt, finalResultInt);

                // catches unique constraint violation if user tries to use a non-unique account name
                try {
                    db.addAccount(account);
                    ArrayList<Integer> list = getDateAndTime();
                    Transaction newTransaction = new Transaction(savedAccountName, finalResultInt, "Income",
                            R.drawable.debit_tracker_launcher_icon, "OPENING BALANCE", savedAccountName, finalResultInt,
                            list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5), true);
                    newTransaction.setTotalBalance(finalResultInt);
                    // try adding new transaction
                    // catch invalid transaction (transaction = null)
                    try {
                        db.addTransaction(newTransaction);
                    } catch (InvalidParameterException e) {
                        e.printStackTrace();
                    }

                    finish();
                }
                catch (SQLiteConstraintException e){
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_account_exists);
                    dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.water_dialog_border));
                    Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            accountName.setText("");
                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

                        }
                    });
                    dialog.show();
                }
            }
        });
    }

    public ArrayList<Integer> getDateAndTime() {
        ArrayList<Integer> list = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = (now.get(Calendar.MONTH) + 1);
        int day = now.get(Calendar.DATE);
        int minute = now.get(Calendar.MINUTE);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int second = now.get(Calendar.SECOND);
        list.add(month);
        list.add(day);
        list.add(year);
        list.add(hour);
        list.add(minute);
        list.add(second);
        return list;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1){
            String result = data.getStringExtra("VALUE");
            int resultInt = Integer.parseInt(result);
            startingBalanceButton.setText(nf.format(resultInt/100.0));
            finalResultInt = resultInt;

        }
    }

//    public void setActionBar() {
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.y_add_account_action_bar);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
