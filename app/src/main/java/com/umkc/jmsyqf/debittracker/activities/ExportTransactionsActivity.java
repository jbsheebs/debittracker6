package com.umkc.jmsyqf.debittracker.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Transition;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.umkc.jmsyqf.debittracker.R;

public class ExportTransactionsActivity extends ActionBarActivity {

    private Toolbar toolbarBottom;

    public void onCreate(final Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        Transition fade = new Fade();
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        fade.excludeTarget(R.id.main_toolbar_bottom, true);
        getWindow().setExitTransition(fade);
        getWindow().setEnterTransition(fade);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.export_layout_view);

        toolbarBottom = (Toolbar) findViewById(R.id.main_toolbar_bottom);
        setSupportActionBar(toolbarBottom);
        toolbarBottom.setTransitionName("action_bar");

        ImageView currentExportView = (ImageView)findViewById(R.id.export_view);
        currentExportView.setImageResource(R.mipmap.export_view_selected);

        ImageView pieChartView = (ImageView)findViewById(R.id.pie_chart_view);
        pieChartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPieChartActivity();
            }
        });

        ImageView mainListView = (ImageView)findViewById(R.id.main_list_view);
        mainListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainListView();
            }
        });

        ImageView searchListView = (ImageView)findViewById(R.id.search_view);
        searchListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSearchViewActivity();
            }
        });
    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToMainListView(){
        Intent i = new Intent(ExportTransactionsActivity.this, MainAccountListViewLayout.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void goToPieChartActivity(){
        Intent i = new Intent(ExportTransactionsActivity.this, PieChartActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void goToSearchViewActivity(){
        Intent i = new Intent(ExportTransactionsActivity.this, SearchResultsActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }




}
