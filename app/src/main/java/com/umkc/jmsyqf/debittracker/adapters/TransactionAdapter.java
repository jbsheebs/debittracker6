package com.umkc.jmsyqf.debittracker.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.R;

import java.text.NumberFormat;
import java.util.ArrayList;

import com.umkc.jmsyqf.debittracker.objects.InvalidParameterException;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

public class TransactionAdapter extends ArrayAdapter<Transaction> {

    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    private ArrayList<Transaction> transactionArrayList;
    private LayoutInflater layoutInflater;
    private Context context;
    private DatabaseHelper db;
    private static final int OPENING_BALANCE = 0;
    private static final int NO_IMAGE_VIEW = 1;
    private static final int OTHER_TRANSACTIONS = 2;

    public TransactionAdapter(Context context, ArrayList<Transaction> results) {
        super(context, R.layout.transaction_row_layout_original_view, results);
        this.transactionArrayList = new ArrayList<>();
        this.transactionArrayList.addAll(results);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        db = new DatabaseHelper(context);

    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        int type;
        if (transactionArrayList.get(position).getTransCategory().equals("OPENING BALANCE")) {
            type = OPENING_BALANCE;
        } else if (transactionArrayList.get(position).getImageResourceId() == 0) {
            type = NO_IMAGE_VIEW;
        } else {
            type = OTHER_TRANSACTIONS;
        }
        return type;
    }

    @Override
    public Transaction getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        int type = getItemViewType(position);

        if (convertView == null) {

            viewHolder = new ViewHolder();

            if (type == OTHER_TRANSACTIONS) {

                convertView = layoutInflater.inflate(R.layout.transaction_row_layout_original_view, parent, false);
                //viewHolder = new ViewHolder();

                viewHolder.clearedBox = (CheckBox) convertView.findViewById(R.id.cbTransRow);
                viewHolder.type = (TextView) convertView.findViewById(R.id.tfTransType);
                viewHolder.date = (TextView) convertView.findViewById(R.id.tfDate);
                viewHolder.transAmount = (TextView) convertView.findViewById(R.id.tfTransAmount);
                viewHolder.balance = (TextView) convertView.findViewById(R.id.tfTotalBalance);
                viewHolder.image = (ImageView) convertView.findViewById(R.id.list_image);
                viewHolder.description = (TextView) convertView.findViewById(R.id.tfDescription);

            } else if (type == NO_IMAGE_VIEW){

                convertView = layoutInflater.inflate(R.layout.transaction_row_layout_no_image, parent, false);
                //viewHolder = new ViewHolder();

                viewHolder.clearedBox = (CheckBox) convertView.findViewById(R.id.cbTransRow);
                viewHolder.type = (TextView) convertView.findViewById(R.id.tfTransType);
                viewHolder.date = (TextView) convertView.findViewById(R.id.tfDate);
                viewHolder.transAmount = (TextView) convertView.findViewById(R.id.tfTransAmount);
                viewHolder.balance = (TextView) convertView.findViewById(R.id.tfTotalBalance);
                viewHolder.description = (TextView) convertView.findViewById(R.id.tfDescription);

            } else {

                convertView = layoutInflater.inflate(R.layout.transaction_row_layout_opening_balance, parent, false);
                //viewHolder = new ViewHolder();

                viewHolder.date = (TextView) convertView.findViewById(R.id.tfDate);
                viewHolder.transAmount = (TextView) convertView.findViewById(R.id.tfTransAmount);
                viewHolder.balance = (TextView) convertView.findViewById(R.id.tfTotalBalance);
                viewHolder.description = (TextView) convertView.findViewById(R.id.tfDescription);
            }

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (type == OTHER_TRANSACTIONS) {

            if (getItem(position).isExpenseType()) {
                viewHolder.transAmount.setTextColor(Color.RED);
            } else {
                viewHolder.transAmount.setTextColor(Color.parseColor("#008000"));
            }

            viewHolder.clearedBox.setChecked(getItem(position).isCleared());
            viewHolder.image.setImageResource(getItem(position).getImageResourceId());
            viewHolder.type.setText(getItem(position).getTransCategory());

            viewHolder.description.setText(getItem(position).getDescriptionWithParens());

//            if (!getItem(position).getTransDescription().isEmpty()) {
//                viewHolder.description.setText("(" + getItem(position).getTransDescription() + ")");
//            }
            viewHolder.date.setText(getItem(position).getDateAndTimeString());
            viewHolder.transAmount.setText(nf.format(getItem(position).getTransAmount() / 100.0));
            viewHolder.balance.setText(nf.format(getItem(position).getTotalBalance() / 100.0));

        } else if (type == NO_IMAGE_VIEW){

            if (getItem(position).isExpenseType()) {
                viewHolder.transAmount.setTextColor(Color.RED);
            } else {
                viewHolder.transAmount.setTextColor(Color.parseColor("#008000"));
            }

            viewHolder.clearedBox.setChecked(getItem(position).isCleared());
            viewHolder.type.setText(getItem(position).getTransCategory());

            viewHolder.description.setText(getItem(position).getDescriptionWithParens());

//            if (!getItem(position).getTransDescription().isEmpty()) {
//                viewHolder.description.setText("(" + getItem(position).getTransDescription() + ")");
//            }
            viewHolder.date.setText(getItem(position).getDateAndTimeString());
            viewHolder.transAmount.setText(nf.format(getItem(position).getTransAmount()/100.0));
            viewHolder.balance.setText(nf.format(getItem(position).getTotalBalance()/100.0));

        } else {

            viewHolder.date.setText(getItem(position).getDateAndTimeString());

            viewHolder.description.setText(getItem(position).getDescriptionWithParens());

//            if (!getItem(position).getTransDescription().isEmpty()) {
//                viewHolder.description.setText("(" + getItem(position).getTransDescription() + ")");
//            }
            viewHolder.transAmount.setTextColor(Color.parseColor("#008000"));
            viewHolder.transAmount.setText(nf.format(getItem(position).getTransAmount()/100.0));
            viewHolder.balance.setText(nf.format(getItem(position).getTotalBalance()/100.0));

        }

        if (type == OTHER_TRANSACTIONS || type == NO_IMAGE_VIEW) {

            final ViewGroup parentViewGroup = parent;

            viewHolder.clearedBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get parent of parent view group --> view --> main_transaction_list_view
                    View view = (View) parentViewGroup.getParent();
                    TextView textViewClearedBalance = (TextView) view.findViewById(R.id.trans_cleared_balance);
                    TextView textViewAccountName = (TextView) view.findViewById(R.id.trans_account_name);
                    String accountName = textViewAccountName.getText().toString();
                    CheckBox cb = (CheckBox) v;
                    Transaction transaction = getItem(position);
                    transaction.setCleared(cb.isChecked());
                    // try updating transaction in DB
                    // catch invalid transaction
                    try {
                        db.updateTransaction(transaction);
                    } catch (InvalidParameterException e) {
                        e.printStackTrace();
                    }
                    int clearedBalanceForTextView;
                    int actualClearedBalance;
                    if (accountName.equals("ALL ACCOUNTS")) {
                        clearedBalanceForTextView = db.getClearedBalancesFromTransactionsForAllAccounts();

                    } else {
                        clearedBalanceForTextView = db.getClearedBalanceFromTransactions(transaction.getOwnerName());
                    }

                    actualClearedBalance = db.getClearedBalanceFromTransactions(transaction.getOwnerName());

                    // update cleared balance
                    textViewClearedBalance.setText(nf.format(clearedBalanceForTextView / 100.0));
                    db.updateAccountClearedBalance(transaction.getOwnerName(), actualClearedBalance);
                }
            });
        }

        return convertView;
    }

    static class ViewHolder {
        TextView type;
        TextView date;
        TextView transAmount;
        TextView balance;
        TextView description;
        ImageView image;
        CheckBox clearedBox;
    }
}
