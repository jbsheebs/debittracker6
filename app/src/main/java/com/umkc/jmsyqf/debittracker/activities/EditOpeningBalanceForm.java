package com.umkc.jmsyqf.debittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.umkc.jmsyqf.debittracker.R;
import com.umkc.jmsyqf.debittracker.controllers.DatabaseGateway;
import com.umkc.jmsyqf.debittracker.controllers.DateTimeHelper;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.InvalidParameterException;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;

public class EditOpeningBalanceForm extends ActionBarActivity {

    private String type;
    private String category;
    private String description;
    private String accountOwnerName;
    private int month;
    private int day;
    private int year;
    private int minute;
    private int hour;
    private int second;
    private String transactionLayoutOwnerName;
    private int imageResourceId;

    private DatabaseGateway databaseGateway;
    private DatabaseHelper db;

    private Button buttonTransactionValue;
    private Button buttonDate;
    private int resultBalanceInt;

    private DateTimeHelper dateTimeHelper;

    private NumberFormat nf;
    private Calendar calendar;
    private final int NUMBER_PAD = 1;
    private final String TRANSACTION_LAYOUT_OWNER_NAME = "TRANSACTION_LAYOUT_NAME";

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_opening_balance_transaction);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_edit_transaction);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        TextView title = (TextView)findViewById(R.id.action_bar_title);
        title.setText("Opening Balance");


        //setActionBar();
        nf = NumberFormat.getCurrencyInstance();
        Intent intent = getIntent();
        transactionLayoutOwnerName = intent.getExtras().getString(TRANSACTION_LAYOUT_OWNER_NAME);
        final Transaction openingTransaction = (Transaction) intent.getExtras().getSerializable("CURRENT_TRANSACTION");

        dateTimeHelper = new DateTimeHelper();
        db = new DatabaseHelper(this);
        databaseGateway = new DatabaseGateway(this);

        buttonDate = (Button) findViewById(R.id.btnDate);
        buttonTransactionValue = (Button) findViewById(R.id.btnTranVal);
        Button saveButton = (Button) findViewById(R.id.btnSave);
        final TextView descriptionTextView = (TextView) findViewById(R.id.txtDesc);

        loadInitialDate();
        calendar = Calendar.getInstance();
        buttonTransactionValue.setText(nf.format(0));

        buttonTransactionValue.setText(nf.format(openingTransaction.getTransAmount() / 100.0));

        // get values for update in DB (irrelevant to this class however)
        accountOwnerName = openingTransaction.getOwnerName();
        type = openingTransaction.getTransType();
        category = openingTransaction.getTransCategory();
        imageResourceId = openingTransaction.getImageResourceId();

        month = openingTransaction.getTransMonth();
        day = openingTransaction.getTransDay();
        year = openingTransaction.getTransYear();
        hour = openingTransaction.getTransHour();
        minute = openingTransaction.getTransMinute();
        second = openingTransaction.getTransSecond();
        buttonDate.setText(openingTransaction.getDateAndTimeString());

        calendar.set(year, month - 1, day, hour, minute, second);

        descriptionTextView.setText(openingTransaction.getTransDescription());
        resultBalanceInt = openingTransaction.getTransAmount();

        buttonTransactionValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditOpeningBalanceForm.this, NumberPadForm.class);
                i.putExtra("VALUE", 0);
                startActivityForResult(i, NUMBER_PAD);
            }
        });

        final SlideDateTimeListener listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {
                calendar.setTime(date);
                month = (calendar.get(Calendar.MONTH) + 1);
                day = calendar.get(Calendar.DATE);
                year = calendar.get(Calendar.YEAR);
                hour = calendar.get(Calendar.HOUR_OF_DAY);
                minute = calendar.get(Calendar.MINUTE);
                second = calendar.get(Calendar.SECOND);
                // load user selected date to time button
                String currentDateTime = dateTimeHelper.loadDate(month, day, year, hour, minute);
                buttonDate.setText(currentDateTime);
            }
        };

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get date object from calendar for date time picker
                Date date = calendar.getTime();
                SlideDateTimePicker.Builder dateTimePicker = new SlideDateTimePicker.Builder(getSupportFragmentManager());
                dateTimePicker.setListener(listener).setInitialDate(date).build().show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                description = descriptionTextView.getText().toString();

                Transaction newTransaction = new Transaction(openingTransaction.getTransID(), accountOwnerName, resultBalanceInt,
                        type, imageResourceId, category, description, month, day, year, hour, minute, second, true);

                // try updating transaction in DB
                // catch invalid transaction
                try {
                    db.updateTransaction(newTransaction);
                } catch (InvalidParameterException e) {
                    e.printStackTrace();
                }

                Intent resultIntent = getIntent();
                // give correct layout back to calling activity
                resultIntent.putExtra(TRANSACTION_LAYOUT_OWNER_NAME, transactionLayoutOwnerName);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    public void loadInitialDate() {

        Calendar now = Calendar.getInstance();
        year = now.get(Calendar.YEAR);
        month = (now.get(Calendar.MONTH) + 1);
        day = now.get(Calendar.DATE);
        minute = now.get(Calendar.MINUTE);
        hour = now.get(Calendar.HOUR_OF_DAY);
        second = now.get(Calendar.SECOND);
        String currentDateTime;
        String amPm = "AM";
        int amPmHour = 0;
        if (hour > 12) {
            amPmHour = hour - 12;
            amPm = "PM";
        } else if (hour == 12) {
            amPmHour = 12;
            amPm = "PM";
        } else if (hour == 0) {
            amPmHour = 12;
            amPm = "AM";
        }
        if (minute < 10) {
            currentDateTime = month + "/" + day + "/" + year + " " + amPmHour + ":0" + minute + " " + amPm;
        } else {
            currentDateTime = month + "/" + day + "/" + year + " " + amPmHour + ":" + minute + " " + amPm;
        }
        buttonDate.setText(currentDateTime);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == NUMBER_PAD) {
            // get number from number pad
            String result = data.getStringExtra("VALUE");
            int resultInt = Integer.parseInt(result);
            buttonTransactionValue.setText(nf.format(resultInt / 100.0));
            resultBalanceInt = resultInt;
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

//    public void setActionBar() {
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.edit_transaction_action_bar);
//        TextView title = (TextView)findViewById(R.id.action_bar_title);
//        title.setText("Opening Balance");
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//    }
}