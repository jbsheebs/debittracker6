package com.umkc.jmsyqf.debittracker.activities;

import android.app.ActivityOptions;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.umkc.jmsyqf.debittracker.R;
import com.umkc.jmsyqf.debittracker.adapters.SearchViewAdapter;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.PieChartCategoryItems;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

import java.util.ArrayList;

public class SearchResultsActivity extends ActionBarActivity {

    private ListView listView;
    private DatabaseHelper db;
    private ArrayList<Transaction> transactionList = new ArrayList<>();
    private SearchView inputSearch;
    SearchViewAdapter adapter;
    private Context context = this;
    private Toolbar toolbarBottom;

    protected void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        Transition fade = new Fade();
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        fade.excludeTarget(R.id.main_toolbar_bottom, true);
        getWindow().setExitTransition(fade);
        getWindow().setEnterTransition(fade);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_view_layout);

        toolbarBottom = (Toolbar) findViewById(R.id.main_toolbar_bottom);
        setSupportActionBar(toolbarBottom);
        toolbarBottom.setTransitionName("action_bar");

        ImageView currentSearchView = (ImageView)findViewById(R.id.search_view);
        currentSearchView.setImageResource(R.mipmap.search_view_selected);

        ImageView pieChartView = (ImageView)findViewById(R.id.pie_chart_view);
        pieChartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPieChartActivity();
            }
        });

        // go to main account list view home "page"
        ImageView mainListView = (ImageView)findViewById(R.id.main_list_view);
        mainListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainListView();
            }
        });

        ImageView exportView = (ImageView)findViewById(R.id.export_view);
        exportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToExportViewActivity();
            }
        });

        db = new DatabaseHelper(this);
        transactionList = db.getAllTransactions();

        listView = (ListView) findViewById(R.id.list_view_search);
        adapter = new SearchViewAdapter(context, transactionList);
        listView.setAdapter(adapter);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        inputSearch = (SearchView) findViewById(R.id.search_view_query);
        inputSearch.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        inputSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                inputSearch.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToMainListView(){
        Intent i = new Intent(SearchResultsActivity.this, MainAccountListViewLayout.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void goToPieChartActivity(){
        Intent i = new Intent(SearchResultsActivity.this, PieChartActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void goToExportViewActivity(){
        Intent i = new Intent(SearchResultsActivity.this, ExportTransactionsActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

}
