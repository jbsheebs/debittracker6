package com.umkc.jmsyqf.debittracker.activities;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import com.umkc.jmsyqf.debittracker.adapters.AccountAdapter;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.Account;
import com.umkc.jmsyqf.debittracker.R;


public class MainAccountListViewLayout extends ActionBarActivity {

    private ArrayList<Account> accountList = new ArrayList<Account>();
    private ListView listView;
    private int accountPosition;
    private Account newAccount = new Account();
    private DatabaseHelper db;
    private Toolbar toolbarBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        Transition fade = new Fade();
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        fade.excludeTarget(R.id.main_toolbar_bottom, true);
        getWindow().setExitTransition(fade);
        getWindow().setEnterTransition(fade);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_list_view);

        Toolbar toolbarTop = (Toolbar)findViewById(R.id.main_toolbar_top);
        setSupportActionBar(toolbarTop);
        toolbarBottom = (Toolbar)findViewById(R.id.main_toolbar_bottom);
        setSupportActionBar(toolbarBottom);
        toolbarBottom.setTransitionName("action_bar");

        db = new DatabaseHelper(this);

        accountList = db.getAllAccounts();

        // add ALL ACCOUNTS item to "top" of list
        if (!accountList.isEmpty()) {
            Account allAccountsAccount = getAndSetBalancesForAllAccounts();
            accountList.add(0, allAccountsAccount);
        }

        ImageView currentListView = (ImageView)findViewById(R.id.main_list_view);
        currentListView.setImageResource(R.mipmap.list_view_selected);

        ImageView searchView = (ImageView)findViewById(R.id.search_view);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView();
            }
        });

        ImageView pieChartView = (ImageView)findViewById(R.id.pie_chart_view);
        pieChartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieChartView();
            }
        });

        ImageView exportView = (ImageView)findViewById(R.id.export_view);
        exportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportView();
            }
        });

        //setActionBar();
        TextView addAccountButton = (TextView) findViewById(R.id.edit_account);
        addAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAccount();
            }
        });

        listView = (ListView) findViewById(R.id.account_list_view);
        listView.setAdapter(new AccountAdapter(this, accountList));

        // register listView for Context Menu
        registerForContextMenu(listView);


        // set listener for ListView
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Object listObject = listView.getItemAtPosition(position);
                //Account fullObject = (Account) listObject;
                // Use a toast message to show which item selected
                String text = "You selected item " + position;
                String name = accountList.get(position).getAccountName();
                int balance = accountList.get(position).getAccountBalance();
                viewTransactions(name);

                Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        accountList = db.getAllAccounts();

        if (!accountList.isEmpty()) {
            Account mainAccount = getAndSetBalancesForAllAccounts();
            accountList.add(0, mainAccount);
        }

        for (int i = 1; i < accountList.size(); i++){
            accountList.get(i).setAccountTotalBalance(db.getTotalBalanceFromTransactions(accountList.get(i).getAccountName()));
            accountList.get(i).setAccountClearedBalance(db.getClearedBalanceFromTransactions(accountList.get(i).getAccountName()));
        }
        listView.setAdapter(new AccountAdapter(this, accountList));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        accountPosition = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater m = getMenuInflater();
        if (accountPosition != 0) {
            m.inflate(R.menu.context_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.open_account:
                Context context = getApplicationContext();
                CharSequence message = "More about the author...";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, message, duration);
                toast.show();
                return true;
            case R.id.edit_account:
                Context context1 = getApplicationContext();
                CharSequence message1 = "Secret Message...";
                int duration1 = Toast.LENGTH_SHORT;
                Toast toast1 = Toast.makeText(context1, message1, duration1);
                toast1.show();
                return true;
            case R.id.delete_account:

                // if I don't keep ALL ACCOUNTS in database then I will need to check on deleting the correct position
                // from the array and the DB

                Context context2 = getApplicationContext();
                CharSequence message2 = "Deleting account " + accountList.get(accountPosition).getAccountName();
                db.deleteAccount(accountList.get(accountPosition));

                if (accountList.size() == 2){
                    accountList.clear();
                } else {

                    accountList.remove(accountPosition);

                    // use these functions to get balances for "ALL ACCOUNTS" not from accounts
                    int actualBalance = db.getTotalBalancesFromTransactionsForAllAccounts();
                    int clearedBalance = db.getClearedBalancesFromTransactionsForAllAccounts();


                    accountList.get(0).setAccountTotalBalance(actualBalance);
                    accountList.get(0).setAccountClearedBalance(clearedBalance);

                }

                listView.setAdapter(new AccountAdapter(this, accountList));
                int duration2 = Toast.LENGTH_SHORT;
                Toast toast2 = Toast.makeText(context2, message2, duration2);
                toast2.show();
                return true;
        }
        return false;
    }

    public Account getAndSetBalancesForAllAccounts(){

        Account newAccount = new Account();
        int totalBalance = db.getTotalBalancesFromTransactionsForAllAccounts();
        int clearedBalance = db.getClearedBalancesFromTransactionsForAllAccounts();
        newAccount.setAccountName("ALL ACCOUNTS");
        newAccount.setAccountTotalBalance(totalBalance);
        newAccount.setAccountClearedBalance(clearedBalance);
        return newAccount;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        accountList = db.getAllAccounts();

        Account allAccountsAccount = getAndSetBalancesForAllAccounts();
        accountList.add(0, allAccountsAccount);

        listView.setAdapter(new AccountAdapter(this, accountList));
        Toast.makeText(MainAccountListViewLayout.this, newAccount.getAccountName(), Toast.LENGTH_SHORT).show();
        Toast.makeText(MainAccountListViewLayout.this, newAccount.getBalanceToString(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // go to transaction layout
    public void viewTransactions(String name){
        Intent i = new Intent(MainAccountListViewLayout.this, TransactionListViewLayout.class);
        i.putExtra("NAME", name);
        //i.putExtra("BALANCE", balance);
        startActivity(i);
    }

    public void pieChartView(){
        Intent i = new Intent(MainAccountListViewLayout.this, PieChartActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void searchView(){
        Intent i = new Intent(MainAccountListViewLayout.this, SearchResultsActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    public void exportView(){
        Intent i = new Intent(MainAccountListViewLayout.this, ExportTransactionsActivity.class);
        ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(this, toolbarBottom, "action_bar");
        startActivity(i, option.toBundle());
    }

    // create new account
    public void addAccount(){
        Intent i = new Intent(MainAccountListViewLayout.this, NewAccountForm.class);
        startActivityForResult(i, 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
