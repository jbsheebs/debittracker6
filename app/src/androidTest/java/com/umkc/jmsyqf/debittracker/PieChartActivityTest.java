package com.umkc.jmsyqf.debittracker;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.umkc.jmsyqf.debittracker.activities.PieChartActivity;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;
import com.umkc.jmsyqf.debittracker.objects.Transaction;

public class PieChartActivityTest extends ActivityInstrumentationTestCase2<PieChartActivity>{

    private PieChartActivity pieChartActivity;
    private DatabaseHelper db;

    public PieChartActivityTest() {
        super(PieChartActivity.class);
    }

    public void setUp(){
        setActivityInitialTouchMode(false);
        pieChartActivity = (PieChartActivity)getActivity();
    }

//    public void testToggle() throws Exception {
//
//        ToggleButton toggleButton;
//        toggleButton = (ToggleButton)pieChartActivity.findViewById(R.id.expense_income_toggle);
//        assertNotNull(toggleButton);
//
//        assertTrue("Toggle should start in non-checked state: ", !toggleButton.isChecked());
//
//        TouchUtils.clickView(this, toggleButton);
//
//        getInstrumentation().waitForIdleSync();
//
//        assertTrue("Toggle should be in opposite state after click: ", toggleButton.isChecked());
//    }

    public void testCenterAmountTextView() throws Exception {

        db = new DatabaseHelper(getActivity());

        TextView totalAmount = (TextView)pieChartActivity.findViewById(R.id.tf_total_amount);

        Transaction testTransaction = new Transaction(1, "John", 2000, "Expense", R.drawable._atm, "ATM", "", 4, 27, 2015, 8, 58, 44, true);

        db.addTransaction(testTransaction);

        assertNotNull("Center total amount should not be null on expense only: ", totalAmount);
    }
}
