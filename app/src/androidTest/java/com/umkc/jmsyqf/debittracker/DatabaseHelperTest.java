package com.umkc.jmsyqf.debittracker;

import android.content.Context;
import android.test.AndroidTestCase;

import com.umkc.jmsyqf.debittracker.objects.Account;
import com.umkc.jmsyqf.debittracker.objects.DatabaseHelper;

import java.util.ArrayList;

public class DatabaseHelperTest extends AndroidTestCase {

    private DatabaseHelper databaseHelper;

    public void setUp(){
        Context c = getContext();
        databaseHelper = new DatabaseHelper(c);
    }

    public void testInsertAccountName() throws Exception {
        Account newAccount = new Account("Michael", 1234, 1234);
        databaseHelper.addAccount(newAccount);
        ArrayList<Account> accountList = databaseHelper.getAllAccounts();
        boolean found = false;
        for (Account a: accountList){
            if (a.getAccountName().equals("Michael")){
                found = true;
                break;
            }
        }
        assertTrue("Issue inserting new Account: ", found);
    }
}
